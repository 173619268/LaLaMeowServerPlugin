package com.lalameow.rpginventorygui.config;


import com.google.common.collect.Lists;
import com.lalameow.common.gui.location.GuiLocation;
import com.lalameow.common.gui.slot.SlotEntity;
import com.lalameow.common.gui.texture.TextureEntity;
import com.lalameow.common.network.GuiPacket;
import com.lalameow.rpginventorygui.RPGInventoryGui;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;
import ru.endlesscode.rpginventory.RPGInventory;
import ru.endlesscode.rpginventory.inventory.slot.Slot;
import ru.endlesscode.rpginventory.inventory.slot.SlotManager;

import java.util.List;

/**
 * Author: SettingDust.
 * Date: 2018/5/8.
 */
public class ConfigManager {
    @Getter
    private static ConfigManager instance;
    @Getter
    private LanguageConfig languageConfig;
    @Getter
    private MainConfig mainConfig;

    public ConfigManager(JavaPlugin plugin) {
        instance = this;
        mainConfig = new MainConfig(plugin);
        languageConfig = new LanguageConfig(mainConfig.LANGUAGE + ".yml", plugin);


        List<Slot> slots;
        List<SlotEntity> slotEntities = Lists.newArrayList();
        //活动
        slots = SlotManager.instance().getActiveSlots();
        for (Slot slot : slots) {
            GuiLocation location = new GuiLocation(slot.getSlotId());
            slotEntities.add(new SlotEntity(location, true));
        }
        //被动
        slots = SlotManager.instance().getPassiveSlots();
        for (Slot slot : slots) {
            GuiLocation location = new GuiLocation(slot.getSlotId());
            slotEntities.add(new SlotEntity(location, true));
        }
        //装备
        slots = SlotManager.instance().getArmorSlots();
        for (Slot slot : slots) {
            GuiLocation location = new GuiLocation(slot.getSlotId());
            slotEntities.add(new SlotEntity(location, true));
        }
        //信息
        slots = SlotManager.instance().getInfoSlots();
        for (Slot slot : slots) {
            GuiLocation location = new GuiLocation(slot.getSlotId());
            slotEntities.add(new SlotEntity(location, true));
        }
        //QuickSlot
        slots = SlotManager.instance().getQuickSlots();
        for (Slot slot : slots) {
            GuiLocation location = new GuiLocation(slot.getSlotId());
            slotEntities.add(new SlotEntity(location, false));
        }
        //盾牌
        Slot slot = SlotManager.instance().getShieldSlot();
        if (slot != null) {
            GuiLocation location = new GuiLocation(slot.getSlotId());
            slotEntities.add(new SlotEntity(location, true));
        }
        //宠物
        slot = SlotManager.instance().getPetSlot();
        if (slot != null) {
            GuiLocation location = new GuiLocation(slot.getSlotId());
            slotEntities.add(new SlotEntity(location, true));
        }

        RPGInventoryGui.getInstance().getRpgGuiPackets().put(
                RPGInventory.getLanguage().getMessage("title"),
                new GuiPacket(
                        new TextureEntity(mainConfig.TEXTURE,
                                0, 0, 0, 0),
                        slotEntities.toArray(new SlotEntity[slotEntities.size()])
                )
        );
        RPGInventoryGui.getInstance().getRpgGuiPackets().put("PlayerInventory",
                new GuiPacket(
                        new TextureEntity(mainConfig.TEXTURE,
                                0, 0, 0, 0),
                        new SlotEntity[0]
                ));
    }
}
