package com.lalameow.rpginventorygui.config;

import com.lalameow.common.config.AbstractConfig;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Author: SettingDust.
 * Date: 2018/5/7.
 */

public class LanguageConfig extends AbstractConfig {

    protected LanguageConfig(String fileName, JavaPlugin plugin) {
        super(fileName, null, plugin);
    }

    @Override
    public void createDefault() {
        createFromResource();
    }

    @Override
    public void initConfig() {
    }
}
