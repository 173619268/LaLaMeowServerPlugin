package com.lalameow.rpginventorygui.config;

import com.lalameow.common.config.AbstractConfig;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Author: SettingDust.
 * Date: 2018/5/7.
 */
public class MainConfig extends AbstractConfig {
    public String LANGUAGE = "zh_CN";
    public String TEXTURE = "skin.png";

    protected MainConfig(JavaPlugin plugin) {
        super("config.yml", null, plugin);
    }

    public void createDefault() {
        this.createFromResource();
    }

    public void initConfig() {
        LANGUAGE = getConfigFile().getString("language", "zh_CN");
        TEXTURE = getConfigFile().getString("texture", "skin.png");
    }
}
