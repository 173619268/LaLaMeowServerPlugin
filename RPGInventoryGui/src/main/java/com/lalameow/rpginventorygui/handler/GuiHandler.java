package com.lalameow.rpginventorygui.handler;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import ru.endlesscode.rpginventory.inventory.InventoryManager;

import java.nio.charset.Charset;

/**
 * Author: SettingDust.
 * Date: 2018/5/18.
 */
public class GuiHandler implements PluginMessageListener {
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (channel.equals("GuiCommon")) {
            String msg = new String(message, Charset.forName("UTF-8"));
            if (msg.equals("OpenRPGInventory")) {
                InventoryManager.get(player).openInventory();
            }
        }
    }
}
