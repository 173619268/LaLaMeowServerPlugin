package com.lalameow.rpginventorygui;

import com.google.common.collect.Maps;
import com.lalameow.common.network.GuiPacket;
import com.lalameow.rpginventorygui.config.ConfigManager;
import com.lalameow.rpginventorygui.listener.PlayerListener;
import com.lalameow.rpginventorygui.handler.GuiHandler;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class RPGInventoryGui extends JavaPlugin {
    @Getter
    private static RPGInventoryGui instance;
    @Getter
    private static ConfigManager configManager;
    @Getter
    private HashMap<String, GuiPacket> rpgGuiPackets = Maps.newHashMap();


    @Override
    public void onEnable() {
        instance = this;
        configManager = new ConfigManager(this);
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "GuiCommon");
        getServer().getMessenger().registerIncomingPluginChannel(this, "GuiCommon", new GuiHandler());
    }

    @Override
    public void onDisable() {

    }
}
