package com.lalameow.rpginventorygui.listener;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.lalameow.common.gui.location.GuiLocation;
import com.lalameow.common.gui.slot.SlotEntity;
import com.lalameow.common.gui.texture.TextureEntity;
import com.lalameow.common.gui.util.GuiUtils;
import com.lalameow.common.network.GuiPacket;
import com.lalameow.rpginventorygui.RPGInventoryGui;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerRegisterChannelEvent;
import org.bukkit.inventory.Inventory;
import ru.endlesscode.rpginventory.inventory.InventoryLocker;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

/**
 * Author: SettingDust.
 * Date: 2018/5/13.
 */
public class PlayerListener implements Listener {
    @EventHandler
    public void onRegister(PlayerRegisterChannelEvent event) {
        if (event.getChannel().equals("GuiCommon")) {
            try {
                Player player = event.getPlayer();
                Inventory inventory = player.getInventory();
                HashMap<String, GuiPacket> guiPackets = Maps.newHashMap(RPGInventoryGui.getInstance().getRpgGuiPackets());
                List<SlotEntity> slotEntities = Lists.newArrayList();
                addQuicks(slotEntities);
                addLocks(inventory, slotEntities);
                GuiPacket guiPacket = new GuiPacket(
                        new TextureEntity(null, 0, 0, 0, 0),
                        slotEntities.toArray(new SlotEntity[0]));

                guiPackets.put("PlayerInventory", guiPacket);

                player.sendPluginMessage(RPGInventoryGui.getInstance(), "GuiCommon", new Gson().toJson(guiPackets).getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            Bukkit.getScheduler().runTask(RPGInventoryGui.getInstance(), () -> {
                Inventory inventory = event.getClickedInventory();
                HashMap<String, GuiPacket> guiPackets = Maps.newHashMap(RPGInventoryGui.getInstance().getRpgGuiPackets());
                try {
                    List<SlotEntity> slotEntities = Lists.newArrayList();
                    addQuicks(slotEntities);
                    addLocks(inventory, slotEntities);
                    GuiPacket guiPacket = new GuiPacket(
                            new TextureEntity(null, 0, 0, 0, 0),
                            slotEntities.toArray(new SlotEntity[0]));

                    guiPackets.put("PlayerInventory", guiPacket);
                    player.sendPluginMessage(RPGInventoryGui.getInstance(), "GuiCommon", new Gson().toJson(guiPackets).getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void addQuicks(List<SlotEntity> slotEntities) {
        for (int i = 0; i < 9; i++) {
            SlotEntity slotEntity = GuiUtils.getSlot(slotEntities, new GuiLocation(i + 36));
            slotEntity.setTexture(new TextureEntity(null, 0, 0, 0, 0));
            slotEntities.remove(slotEntity);
            slotEntities.add(slotEntity);
        }
    }

    private void addLocks(Inventory inventory, List<SlotEntity> slotEntities) {
        boolean flag = false;
        for (int i = 0; i < 36; i++) {
            SlotEntity slotEntity = GuiUtils.getSlot(slotEntities, new GuiLocation(i));

            if (InventoryLocker.isLockedSlot(inventory.getItem(i)) && !flag) {
                flag = true;
                slotEntity.setTexture(new TextureEntity("buy.png", 0, 0, 0, 0));
                slotEntities.remove(slotEntity);
                slotEntities.add(slotEntity);
            } else if (i > 8) {
                slotEntity = GuiUtils.getSlot(slotEntities, new GuiLocation(i));
                slotEntity.setTexture(new TextureEntity(null, 0, 0, 0, 0));
                slotEntities.remove(slotEntity);

                for (SlotEntity entity : slotEntities) {
                    if (entity.getTexture().getPath() != null && entity.getTexture().getPath().equals("buy.png")) {
                        if (InventoryLocker.isLockedSlot(inventory.getItem(i)) && !slotEntities.contains(slotEntity)) {
                            slotEntities.remove(slotEntity);
                            slotEntity.setTexture(new TextureEntity("lock.png", 0, 0, 0, 0));
                            slotEntity.setCanFire(false);
                        }
                        break;
                    }
                }
                slotEntities.add(slotEntity);
            }
        }
    }
}
