package com.lalameow.loreattr.attribute.attribute.valueattribute.listener;

import com.lalameow.common.util.LaLaMeowUtil;
import com.lalameow.loreattr.attribute.AttributeManager;
import com.lalameow.loreattr.attribute.util.LoreUtils;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Author: SettingDust
 * Date: 2017/6/28.
 */
public class ListenerDamage implements Listener {
    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if ((event.isCancelled())) return;
        double damage = 0;
        if (event.getEntity() instanceof LivingEntity) {
            LivingEntity entity = (LivingEntity) event.getEntity();
            if (event.getDamager() instanceof LivingEntity) {
                LivingEntity damager = (LivingEntity) event.getDamager();
                double damageBonus = AttributeManager.damage.getAttribute(damager);
                double armor = AttributeManager.armor.getAttribute(damager);
                double critDamage = AttributeManager.critDamage.getAttribute(damager);
                if (AttributeManager.dodge.isDodge(entity)) {
                    event.setCancelled(true);
                    return;
                }
                if (LaLaMeowUtil.random(AttributeManager.critChance.getAttribute(damager))) {
                    critDamage *= 1 - AttributeManager.critDefense.getAttribute(damager);
                    damageBonus += critDamage;
                }
                if (AttributeManager.damage.useRangeOfDamage(damager))
                    damage = Math.max(0.0D, damageBonus - armor);
                else
                    damage = Math.max(0.0D, event.getDamage() + damageBonus - armor);
                event.setDamage(damage);
                double steal = Math.min(damager.getMaxHealth(), damager.getHealth() + Math.min(AttributeManager.lifeSteal.getAttribute(damager), event.getDamage()));
                if (steal >= 0 && steal <= damager.getMaxHealth())
                    damager.setHealth(Math.min(damager.getMaxHealth(), damager.getHealth() + Math.min(AttributeManager.lifeSteal.getAttribute(damager), event.getDamage())));
            } else if (event.getDamager() instanceof Arrow) {
                Arrow arrow = (Arrow) event.getDamager();
                if ((arrow.getShooter() != null) && ((arrow.getShooter() instanceof LivingEntity))) {
                    LivingEntity damager = (LivingEntity) arrow.getShooter();
                    double damageBonus = AttributeManager.damage.getAttribute(damager);
                    double armor = AttributeManager.armor.getAttribute(damager);
                    double critDamage = AttributeManager.critDamage.getAttribute(damager);
                    if (AttributeManager.dodge.isDodge(entity)) {
                        event.setCancelled(true);
                        return;
                    }
                    if (LaLaMeowUtil.random(AttributeManager.critChance.getAttribute(damager))) {
                        critDamage *= 1 - AttributeManager.critDefense.getAttribute(damager);
                        damageBonus += critDamage;
                    }
                    if (AttributeManager.damage.useRangeOfDamage(damager))
                        damage = Math.max(0.0D, damageBonus - armor);
                    else
                        damage = Math.max(0.0D, event.getDamage() + damageBonus - armor);
                    event.setDamage(damage);
                    double steal = Math.min(damager.getMaxHealth(), damager.getHealth() + Math.min(AttributeManager.lifeSteal.getAttribute(damager), event.getDamage()));
                    if (steal >= 0 && steal <= damager.getMaxHealth())
                        damager.setHealth(Math.min(damager.getMaxHealth(), damager.getHealth() + Math.min(AttributeManager.lifeSteal.getAttribute(damager), event.getDamage())));
                }
            }
        }
    }
}
