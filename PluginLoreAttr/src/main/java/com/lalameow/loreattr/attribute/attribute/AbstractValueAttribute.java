package com.lalameow.loreattr.attribute.attribute;

import com.lalameow.loreattr.attribute.entity.EntityAttribute;
import com.lalameow.loreattr.attribute.implement.ValueAttribute;
import com.lalameow.loreattr.attribute.util.LoreUtils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public abstract class AbstractValueAttribute extends AbstractAttribute implements ValueAttribute {
    public AbstractValueAttribute(EntityAttribute entityAttribute) {
        super(entityAttribute);
    }

    public double getAttribute(LivingEntity entity) {
        if (!entity.isValid()) return 0;
        return getValue(LoreUtils.getLore(entity, true, true));
    }

    public double getAttribute(ItemStack itemStack) {
        return getValue(LoreUtils.getLore(itemStack));
    }

}
