package com.lalameow.loreattr.attribute.attribute.limitattribute;

import com.lalameow.common.message.LaLaMeowMessage;
import com.lalameow.loreattr.attribute.attribute.AbstractLimitAttribute;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/7/1.
 */
public class AttributeLevel extends AbstractLimitAttribute {
    private final Pattern levelRegex = Pattern.compile(entityAttribute.getKeyword() + "[ ](\\d+)");

    public AttributeLevel() {
        super(EntityAttribute.getAttribute("level"));
    }

    @Override
    public String getValue(List<String> lore) {
        String level = new String();
        for (String s : lore) {
            Matcher valueMatcher = levelRegex.matcher(s);
            if (valueMatcher.find()) {
                level = valueMatcher.group(2);
                break;
            }
        }
        return level;
    }

    public boolean arrivedLevel(Player player, ItemStack itemStack) {
        if (player.getLevel() < Integer.parseInt(getAttribute(itemStack))) {
            if (entityAttribute.isDisplay())
                LaLaMeowMessage.sendError(player, entityAttribute.getMessage());
            return false;
        }
        return true;
    }
}
