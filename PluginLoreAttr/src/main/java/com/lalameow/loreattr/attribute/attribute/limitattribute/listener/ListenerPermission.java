package com.lalameow.loreattr.attribute.attribute.limitattribute.listener;

import com.lalameow.loreattr.LoreAttr;
import com.lalameow.loreattr.attribute.AttributeManager;
import com.lalameow.loreattr.attribute.util.LoreUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public class ListenerPermission implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (!event.hasItem()) return;
        if (!canUse(event.getPlayer(), event.getItem())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void checkBowRestriction(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        if (!canUse((Player) event.getEntity(), event.getBow()))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void checkCraftRestriction(CraftItemEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }
        for (ItemStack item : event.getInventory().getContents())
            if (!canUse((Player) event.getWhoClicked(), item)) {
                event.setCancelled(true);
                return;
            }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void checkWeaponRestriction(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) return;
        if (!canUse((Player) event.getDamager(), ((Player) event.getDamager()).getItemInHand())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void checkArmorRestriction(PlayerInteractEvent event) {
        final Player livingEntity = event.getPlayer();
        final ItemStack[] armors = livingEntity.getEquipment().getArmorContents();
        final ItemStack fitem = event.getItem();
        Bukkit.getScheduler().runTaskLater(LoreAttr.plugin, new Runnable() {
            public void run() {
                ItemStack[] nowArmors = livingEntity.getEquipment().getArmorContents();
                for (int i = 0; i < armors.length; i++) {
                    if (nowArmors != null
                            && !nowArmors[i].getType().equals(Material.AIR)) {
                        if (!canUse(livingEntity, fitem) &&
                                !LoreUtils.itemIsSimilar(armors[i], nowArmors[i])) {
                            nowArmors[i] = armors[i];
                            fitem.setAmount(1);
                            livingEntity.getInventory().addItem(fitem);
                            livingEntity.getEquipment().setArmorContents(nowArmors);
                        }
                        if (!canUse(livingEntity, nowArmors[i])) {
                            livingEntity.getInventory().addItem(nowArmors[i]);
                            nowArmors[i] = null;
                            livingEntity.getEquipment().setArmorContents(nowArmors);
                        }
                    }
                }
            }
        }, 0L);
    }

    private boolean canUse(Player player, ItemStack itemStack) {
        return AttributeManager.restriction.havePermission(player, itemStack)
                && AttributeManager.level.arrivedLevel(player, itemStack);
    }
}
