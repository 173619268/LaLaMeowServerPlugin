package com.lalameow.loreattr.attribute.attribute;

import com.lalameow.loreattr.LoreAttr;
import com.lalameow.loreattr.attribute.attribute.limitattribute.listener.ListenerPermission;
import com.lalameow.loreattr.attribute.attribute.valueattribute.listener.ListenerAttackSpeed;
import com.lalameow.loreattr.attribute.attribute.valueattribute.listener.ListenerDamage;
import com.lalameow.loreattr.attribute.attribute.valueattribute.listener.ListenerHealth;
import com.lalameow.loreattr.attribute.attribute.valueattribute.listener.ListenerRegen;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: SettingDust
 * Date: 2017/6/28.
 */
public class ListenerManager {
    private static List<Listener> listeners = new ArrayList<>();

    public static void init() {
        listeners.add(new ListenerDamage());
        listeners.add(new ListenerHealth());
        listeners.add(new ListenerAttackSpeed());
        listeners.add(new ListenerRegen());

        listeners.add(new ListenerPermission());

        for (Listener listener : listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, LoreAttr.plugin);
        }
    }
}
