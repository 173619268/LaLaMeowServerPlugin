package com.lalameow.loreattr.attribute.attribute.valueattribute;

import com.lalameow.loreattr.attribute.attribute.AbstractValueAttribute;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public class AttributeCritChance extends AbstractValueAttribute {
    private final Pattern critChanceRegex = Pattern.compile("[+](\\d+)[%][ ](" + entityAttribute.getKeyword() + ")");

    public AttributeCritChance() {
        super(EntityAttribute.getAttribute("critChance"));
    }

    @Override
    public double getValue(List<String> lore) {
        Integer chance = 0;
        for (String s : lore) {
            Matcher valueMatcher = critChanceRegex.matcher(s.toLowerCase());
            if (valueMatcher.find()) {
                chance += Integer.valueOf(valueMatcher.group(1));
            }
        }
        return chance;
    }
}
