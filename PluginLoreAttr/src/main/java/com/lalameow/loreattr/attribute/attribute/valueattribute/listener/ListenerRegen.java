package com.lalameow.loreattr.attribute.attribute.valueattribute.listener;

import com.lalameow.loreattr.attribute.AttributeManager;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;

/**
 * Author: SettingDust
 * Date: 2017/6/28.
 */
public class ListenerRegen implements Listener {
    @EventHandler(priority = EventPriority.NORMAL)
    public void applyHealthRegen(EntityRegainHealthEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (((event.getEntity() instanceof LivingEntity)) &&
                event.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED) {

            LivingEntity entity = (LivingEntity) event.getEntity();
            if (!entity.isValid()) {
                return;
            }
            event.setAmount(event.getAmount() + AttributeManager.regen.getAttribute(entity));

            if (event.getAmount() <= 0.0D)
                event.setCancelled(true);
        }
    }
}
