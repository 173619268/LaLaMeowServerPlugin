package com.lalameow.loreattr.attribute.attribute;

import com.lalameow.loreattr.attribute.entity.EntityAttribute;
import com.lalameow.loreattr.attribute.implement.Attribute;

/**
 * Author: SettingDust
 * Date: 2017/6/22.
 */
public class AbstractAttribute implements Attribute {
    protected EntityAttribute entityAttribute;

    public AbstractAttribute(EntityAttribute entityAttribute) {
        this.entityAttribute = entityAttribute;
    }
}
