package com.lalameow.loreattr.attribute.attribute.valueattribute;

import com.lalameow.loreattr.attribute.attribute.AbstractValueAttribute;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;
import com.lalameow.loreattr.attribute.util.LoreUtils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/6/28.
 */
public class AttributeRegen extends AbstractValueAttribute {
    private final Pattern regen = Pattern.compile("[+](\\d+)[ ](" + entityAttribute.getKeyword() + ")");

    public AttributeRegen() {
        super(EntityAttribute.getAttribute("regen"));
    }

    @Override
    public double getAttribute(LivingEntity entity) {
        if (!entity.isValid()) {
            return 0;
        }
        return getValue(LoreUtils.getLore(entity, true, true));
    }

    @Override
    public double getAttribute(ItemStack itemStack) {
        return getValue(LoreUtils.getLore(itemStack));
    }

    @Override
    public double getValue(List<String> lore) {
        Integer regenBonus = 0;
        for (String s : lore) {
            Matcher matcher = regen.matcher(s.toLowerCase());
            if (matcher.find()) {
                regenBonus = regenBonus + Integer.valueOf(matcher.group(1));
            }
        }
        return regenBonus;
    }
}
