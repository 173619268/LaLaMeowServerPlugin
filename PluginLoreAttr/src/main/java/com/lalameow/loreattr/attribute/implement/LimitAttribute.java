package com.lalameow.loreattr.attribute.implement;

import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Author: SettingDust
 * Date: 2017/6/22.
 */
public interface LimitAttribute extends Attribute {
    String getAttribute(LivingEntity entity);

    String getAttribute(ItemStack itemStack);

    String getValue(List<String> lore);
}
