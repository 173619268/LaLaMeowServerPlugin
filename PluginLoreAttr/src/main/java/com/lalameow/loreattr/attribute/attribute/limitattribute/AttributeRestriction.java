package com.lalameow.loreattr.attribute.attribute.limitattribute;

import com.lalameow.common.message.LaLaMeowMessage;
import com.lalameow.loreattr.attribute.AttributeManager;
import com.lalameow.loreattr.attribute.attribute.AbstractLimitAttribute;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public class AttributeRestriction extends AbstractLimitAttribute {
    private final Pattern restrictionRegex = Pattern.compile("(" + entityAttribute.getKeyword() + ": )(\\w*)");
    private final String permPrefix = "loreattr.item.";

    public AttributeRestriction() {
        super(EntityAttribute.getAttribute("restriction"));
    }

    @Override
    public String getValue(List<String> lore) {
        String perm = new String();
        for (String s : lore) {
            Matcher matcher = restrictionRegex.matcher(s);
            if (matcher.find()) {
                perm = matcher.group(2);
                break;
            }
        }
        return perm;
    }

    public boolean havePermission(Player player, ItemStack itemStack) {
        if (!player.hasPermission(permPrefix + AttributeManager.restriction.getAttribute(itemStack))) {
            if (entityAttribute.isDisplay())
                LaLaMeowMessage.sendError(player, entityAttribute.getMessage());
            return false;
        }
        return true;
    }
}
