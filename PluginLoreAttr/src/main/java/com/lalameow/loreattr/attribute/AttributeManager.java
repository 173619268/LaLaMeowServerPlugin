package com.lalameow.loreattr.attribute;


import com.lalameow.loreattr.attribute.attribute.AttributeLifeSteal;
import com.lalameow.loreattr.attribute.attribute.limitattribute.AttributeLevel;
import com.lalameow.loreattr.attribute.attribute.limitattribute.AttributeRestriction;
import com.lalameow.loreattr.attribute.attribute.valueattribute.*;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;
import com.lalameow.loreattr.config.ConfigManager;

import java.util.Map;

/**
 * Author: SettingDust
 * Date: 2017/6/21.
 */
public class AttributeManager {
    public static AttributeHealth health;
    public static AttributeDamage damage;
    public static AttributeRegen regen;
    public static AttributeAttackSpeed attackSpeed;
    public static AttributeDodge dodge;
    public static AttributeCritChance critChance;
    public static AttributeCritDamage critDamage;
    public static AttributeCritDefense critDefense;
    public static AttributeLifeSteal lifeSteal;
    public static AttributeArmor armor;
    public static AttributeRestriction restriction;
    public static AttributeLevel level;

    public static void init() {
        //TODO Add all attributes
        for (String s : ConfigManager.configKeyword.getConfigFile().getKeys(false)) {
            Map<String, Object> map = ConfigManager.configKeyword.getConfigFile().getConfigurationSection(s).getValues(false);
            EntityAttribute.entityAttributeMap.put(s, new EntityAttribute(map));
        }

        /*数值属性*/
        health = new AttributeHealth();
        damage = new AttributeDamage();
        regen = new AttributeRegen();
        attackSpeed = new AttributeAttackSpeed();
        dodge = new AttributeDodge();
        critChance = new AttributeCritChance();
        critDamage = new AttributeCritDamage();
        critDefense = new AttributeCritDefense();
        lifeSteal = new AttributeLifeSteal();
        armor = new AttributeArmor();
        restriction = new AttributeRestriction();
        level = new AttributeLevel();
    }
}
