package com.lalameow.loreattr.attribute.attribute;

import com.lalameow.loreattr.attribute.entity.EntityAttribute;
import com.lalameow.loreattr.attribute.implement.LimitAttribute;
import com.lalameow.loreattr.attribute.util.LoreUtils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public abstract class AbstractLimitAttribute extends AbstractAttribute implements LimitAttribute {
    public AbstractLimitAttribute(EntityAttribute entityAttribute) {
        super(entityAttribute);
    }

    @Override
    public String getAttribute(LivingEntity entity) {
        if (!entity.isValid()) return new String();
        return getValue(LoreUtils.getLore(entity, true, true));
    }

    @Override
    public String getAttribute(ItemStack itemStack) {
        return getValue(LoreUtils.getLore(itemStack));
    }
}
