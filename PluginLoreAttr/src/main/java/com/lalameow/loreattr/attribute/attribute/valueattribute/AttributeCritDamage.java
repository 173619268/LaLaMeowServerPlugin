package com.lalameow.loreattr.attribute.attribute.valueattribute;

import com.lalameow.loreattr.attribute.attribute.AbstractValueAttribute;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public class AttributeCritDamage extends AbstractValueAttribute {
    private final Pattern critDamageRegex = Pattern.compile("[+](\\d+)[ ](" + entityAttribute.getKeyword() + ")");

    public AttributeCritDamage() {
        super(EntityAttribute.getAttribute("critDamage"));
    }

    @Override
    public double getValue(List<String> lore) {
        Integer damage = 0;
        for (String s : lore) {
            Matcher valueMatcher = critDamageRegex.matcher(s.toLowerCase());
            if (valueMatcher.find()) {
                damage += Integer.valueOf(valueMatcher.group(1));
            }
        }
        return damage;
    }
}
