package com.lalameow.loreattr.attribute.attribute.valueattribute;

import com.lalameow.loreattr.attribute.attribute.AbstractValueAttribute;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public class AttributeArmor extends AbstractValueAttribute {
    private final Pattern armorRegex = Pattern.compile("[+](\\d+)[ ](" + entityAttribute.getKeyword() + ")");

    public AttributeArmor() {
        super(EntityAttribute.getAttribute("armor"));
    }

    @Override
    public double getValue(List<String> lore) {
        Integer armor = 0;
        for (String s : lore) {
            Matcher valueMatcher = armorRegex.matcher(s.toLowerCase());
            if (valueMatcher.find()) {
                armor += Integer.valueOf(valueMatcher.group(1));
            }
        }
        return armor;
    }
}
