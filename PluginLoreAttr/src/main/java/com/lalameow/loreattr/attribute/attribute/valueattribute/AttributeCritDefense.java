package com.lalameow.loreattr.attribute.attribute.valueattribute;

import com.lalameow.loreattr.attribute.attribute.AbstractValueAttribute;
import com.lalameow.loreattr.attribute.entity.EntityAttribute;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public class AttributeCritDefense extends AbstractValueAttribute {
    private final Pattern criticalDefenseRegex = Pattern.compile("[+](\\d+)[%][ ](" + entityAttribute.getKeyword() + ")");

    public AttributeCritDefense() {
        super(EntityAttribute.getAttribute("critDefense"));
    }

    @Override
    public double getValue(List<String> lore) {
        double defense = 0.0D;
        for (String s : lore) {
            Matcher valueMatcher = criticalDefenseRegex.matcher(s.toLowerCase());
            if (valueMatcher.find()) {
                defense += Integer.valueOf(valueMatcher.group(1)) / 100D;
            }
        }
        return defense;
    }
}
