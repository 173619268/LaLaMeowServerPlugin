package com.lalameow.loreattr.attribute.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: SettingDust
 * Date: 2017/6/22.
 */
@Data
public class EntityAttribute {
    public static Map<String, EntityAttribute> entityAttributeMap = new HashMap<>();

    private String message;
    private String keyword;
    private boolean display;

    public EntityAttribute(Map<String, Object> map) {
        if (map.containsKey("message"))
            this.message = String.valueOf(map.get("message"));
        if (map.containsKey("keyword"))
            this.keyword = String.valueOf(map.get("keyword"));
        if (map.containsKey("display"))
            this.display = (boolean) map.get("display");
    }

    public static EntityAttribute getAttribute(String attrName) {
        return entityAttributeMap.containsKey(attrName)
                ? entityAttributeMap.get(attrName)
                : null;
    }
}
