package com.lalameow.loreattr.attribute.attribute.valueattribute.listener;

import com.lalameow.loreattr.attribute.AttributeManager;
import com.lalameow.loreattr.attribute.attribute.valueattribute.AttributeHealth;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * Author: SettingDust
 * Date: 2017/6/28.
 */
public class ListenerHealth implements Listener {
    private AttributeHealth health = AttributeManager.health;

    @EventHandler
    public void onCommand(AsyncPlayerChatEvent event) {
        health.applyHp(event.getPlayer());
    }

    @EventHandler
    public void onEntityInteract(EntityInteractEvent event) {
        if (event.getEntity() instanceof LivingEntity)
            health.applyHp((LivingEntity) event.getEntity());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        health.applyHp(event.getPlayer());
    }

    @EventHandler
    public void onChange(PlayerItemHeldEvent event) {
        AttributeInstance attribute = event.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH);
        AttributeModifier attributeModifier = new AttributeModifier("loreattr_health", health.getAttribute(event.getPlayer().getInventory().getItem(event.getNewSlot())), AttributeModifier.Operation.ADD_NUMBER);
        for (AttributeModifier modifier : attribute.getModifiers()) {
            if (modifier.getName().equals("loreattr_health")) {
                attribute.removeModifier(modifier);
            }
        }
        health.applyHp(event.getPlayer());
        attribute.addModifier(attributeModifier);
        attributeModifier = new AttributeModifier("loreattr_health", -health.getAttribute(event.getPlayer().getInventory().getItem(event.getPreviousSlot())), AttributeModifier.Operation.ADD_NUMBER);
        attribute.addModifier(attributeModifier);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        health.applyHp(event.getPlayer());
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        health.applyHp(event.getPlayer());
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        health.applyHp(event.getEntity());
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof LivingEntity)
            health.applyHp((LivingEntity) event.getEntity());
    }

    @EventHandler
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof LivingEntity)
            health.applyHp((LivingEntity) event.getDamager());
    }
}
