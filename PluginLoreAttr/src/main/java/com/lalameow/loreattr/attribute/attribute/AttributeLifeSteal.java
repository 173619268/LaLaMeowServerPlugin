package com.lalameow.loreattr.attribute.attribute;

import com.lalameow.loreattr.attribute.entity.EntityAttribute;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: SettingDust
 * Date: 2017/6/30.
 */
public class AttributeLifeSteal extends AbstractValueAttribute {
    private final Pattern lifestealRegex = Pattern.compile("[+](\\d+)[ ](" + entityAttribute.getKeyword() + ")");

    public AttributeLifeSteal() {
        super(EntityAttribute.getAttribute("lifeSteal"));
    }

    @Override
    public double getValue(List<String> lore) {
        Integer steal = 0;
        for (String s : lore) {
            Matcher valueMatcher = lifestealRegex.matcher(s.toLowerCase());
            if (valueMatcher.find()) {
                steal += Integer.valueOf(valueMatcher.group(1));
            }
        }
        return steal;
    }
}
