package com.lalameow.loreattr.attribute.implement;

import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Author: SettingDust
 * Date: 2017/6/22.
 */
public interface ValueAttribute extends Attribute {
    double getAttribute(LivingEntity entity);

    double getAttribute(ItemStack itemStack);

    double getValue(List<String> lore);
}
