package com.lalameow.loreattr.attribute.attribute.valueattribute.listener;

import com.lalameow.loreattr.attribute.AttributeManager;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Author: SettingDust
 * Date: 2017/6/28.
 */
public class ListenerAttackSpeed implements Listener {

    @EventHandler
    public void onEntityInteract(EntityInteractEvent event) {
        if (event.getEntity() instanceof LivingEntity) {
            AttributeManager.attackSpeed.applySpeed((LivingEntity) event.getEntity());
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        AttributeManager.attackSpeed.applySpeed(event.getPlayer());
    }
}
