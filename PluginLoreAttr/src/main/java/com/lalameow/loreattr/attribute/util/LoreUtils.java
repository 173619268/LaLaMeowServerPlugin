package com.lalameow.loreattr.attribute.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: SettingDust
 * Date: 2017/6/21.
 */
public class LoreUtils {
    public static List<String> getLore(LivingEntity entity, boolean armor, boolean offhand) {
        List lore = new ArrayList();
        if (armor) {
            lore.addAll(getLore(entity.getEquipment().getArmorContents()));
        }
        ItemStack item;
        item = entity.getEquipment().getItemInMainHand();
        lore.addAll(getLore(item));
        if (offhand) {
            item = entity.getEquipment().getItemInOffHand();
            lore.addAll(getLore(item));
        }
        return lore;
    }

    public static List<String> getLore(ItemStack[] items) {
        List<String> lore = new ArrayList<>();
        for (ItemStack item : items) {
            if ((item != null) &&
                    (item.hasItemMeta()) &&
                    (item.getItemMeta().hasLore())) {
                lore.addAll(getLore(item));
            }
        }
        return lore;
    }

    public static List<String> getLore(ItemStack item) {
        List<String> lore = new ArrayList<>();
        if ((item != null) &&
                (item.hasItemMeta()) &&
                (item.getItemMeta().hasLore())) {
            for (String s : item.getItemMeta().getLore()) {
                lore.add(ChatColor.stripColor(s));
            }
        }
        return lore;
    }


    public static boolean itemIsSimilar(ItemStack item1, ItemStack item2) {
        boolean similar = false;
        if (item1 != null
                && !item1.getType().equals(Material.AIR)
                && item2 != null
                && !item2.getType().equals(Material.AIR)) {
            similar = item1.getDurability() == item2.getDurability()
                    && item1.getType().equals(item2.getType());
            if (item1.hasItemMeta() && item2.hasItemMeta())
                similar = similar && item1.getItemMeta().equals(item2.getItemMeta());

        }
        return similar;
    }
}
