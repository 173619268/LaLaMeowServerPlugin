package com.lalameow.loreattr.command;

import com.lalameow.common.command.AbstractCommand;
import com.lalameow.common.message.LaLaMeowMessage;
import com.lalameow.loreattr.config.ConfigManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Author: SettingDust
 * Date: 2017/6/21.
 */
public class CommandLoreattr extends AbstractCommand {
    @Override
    protected void init() {
        command = "loreattr";
        commands.put("stats", "显示当前属性。");
        commands.put("reload", "重载插件数据。");
    }

    @Override
    protected void showHelp() {
        sender.sendMessage(ChatColor.GREEN + "LoreAttributes帮助信息");
        for (String s : commands.keySet()) {
            sender.sendMessage(ChatColor.GREEN + "/" + s + " " + ChatColor.BOLD + commands.get(s));
        }
    }

    public void reload_cmd() {
        ConfigManager.init();
        LaLaMeowMessage.sendSuccess(sender, "[LoreAttr] 重载成功！");
    }

    public void stats_cmd() {
        //TODO Send stats
        if (sender instanceof Player) {
            Player player = (Player) sender;
        } else {
            LaLaMeowMessage.sendError(sender, "此命令必须由玩家使用！");
        }
    }
}
