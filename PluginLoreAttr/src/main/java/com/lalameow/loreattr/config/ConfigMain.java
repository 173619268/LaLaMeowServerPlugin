package com.lalameow.loreattr.config;

import com.lalameow.common.config.AbstractConfig;
import com.lalameow.loreattr.LoreAttr;
import org.bukkit.configuration.InvalidConfigurationException;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Author: SettingDust
 * Date: 2017/6/20.
 */
public class ConfigMain extends AbstractConfig {
    protected ConfigMain() {
        super("config.yml", null, LoreAttr.plugin);
    }

    public void createDefault() {
        try {
            this.getConfigFile().load(new InputStreamReader(LoreAttr.plugin.getResource(this.getFileName()), "UTF-8"));
            this.save();
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            LoreAttr.plugin.getLogger().warning("config.yml created is failed. ");
        }
    }

    public void initConfig() {

    }
}
