package com.lalameow.loreattr.config;

import com.lalameow.loreattr.LoreAttr;
import com.lalameow.loreattr.attribute.AttributeManager;

/**
 * Author: SettingDust
 * Date: 2017/6/21.
 */
public class ConfigManager {
    public static ConfigMain configMain;
    public static ConfigKeyword configKeyword;

    public static void init() {
        LoreAttr.plugin.getLogger().info("加载LoreAttr配置文件. ");

        configMain = new ConfigMain();
        configKeyword = new ConfigKeyword();
    }
}
