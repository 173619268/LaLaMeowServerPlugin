package com.lalameow.loreattr.config;

import com.lalameow.common.config.AbstractConfig;
import com.lalameow.loreattr.LoreAttr;
import com.lalameow.loreattr.attribute.AttributeManager;
import org.bukkit.configuration.InvalidConfigurationException;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Author: SettingDust
 * Date: 2017/6/21.
 */
public class ConfigKeyword extends AbstractConfig {
    protected ConfigKeyword() {
        super("keyword.yml", null, LoreAttr.plugin);
    }

    @Override
    public void createDefault() {
        try {
            this.getConfigFile().load(new InputStreamReader(LoreAttr.plugin.getResource(this.getFileName()), "UTF-8"));
            this.save();
        } catch (IOException e) {
            e.printStackTrace();
            LoreAttr.plugin.getLogger().warning("keyword.yml created is failed. ");
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            LoreAttr.plugin.getLogger().warning("keyword.yml created is failed. ");
        }
    }

    @Override
    public void initConfig() {

    }
}
