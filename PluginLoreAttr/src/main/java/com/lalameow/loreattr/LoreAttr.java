package com.lalameow.loreattr;

import com.lalameow.loreattr.attribute.AttributeManager;
import com.lalameow.loreattr.attribute.attribute.ListenerManager;
import com.lalameow.loreattr.command.CommandLoreattr;
import com.lalameow.loreattr.config.ConfigManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Author: SettingDust
 * Date: 2017/6/20.
 */
public class LoreAttr extends JavaPlugin {
    public static LoreAttr plugin;

    public void onEnable() {
        plugin = this;

        ConfigManager.init();
        AttributeManager.init();
        ListenerManager.init();

        this.getCommand("loreattr").setExecutor(new CommandLoreattr());


        this.getLogger().info("已经加载");
    }

    public void onDisable() {
    }
}
