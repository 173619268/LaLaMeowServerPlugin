package com.lalameow.smithing.enumeration;

import lombok.Getter;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/7
 * 时间: 18:44
 * 功能：请进行修改
 */
public enum FuelOperType {
    ADDNBT("添加NBT"),
    DELNBT("删除NBT"),
    STRENGTHEN("强化"),
    UPGRADE("升级"),
    ADDHOLE("打孔"),
    INLAY("镶嵌");

    @Getter
    private String name;

    FuelOperType(String name) {
        this.name = name;
    }
    public static FuelOperType getByName(String typeName){
        for (FuelOperType fuelOperType : FuelOperType.values()) {
            if(fuelOperType.getName().equals(typeName)){
                return fuelOperType;
            }
        }
        return null;
    }
}
