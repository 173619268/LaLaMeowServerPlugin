package com.lalameow.smithing.enumeration;

import lombok.Getter;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/7
 * 时间: 19:09
 * 功能：请进行修改
 */
public enum EquimentType {

    WEAPON("武器"),
    ARMOR("防具");

    @Getter
    private String typeName;

    EquimentType(String typeName) {
        this.typeName = typeName;
    }
    public static EquimentType getByTypeName(String typeName){
        for (EquimentType equimentType : EquimentType.values()) {
            if(equimentType.getTypeName().equals(typeName)){
                return equimentType;
            }
        }
        return null;
    }
}
