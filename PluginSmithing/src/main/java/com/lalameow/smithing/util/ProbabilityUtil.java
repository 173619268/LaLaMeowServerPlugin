package com.lalameow.smithing.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/2
 * 时间: 1:45
 * 功能：概率计算
 */
public class ProbabilityUtil {

    public static double calRate(double baseRate,double rateDM,int count){
        double dm=1;
        if(count>0 && rateDM>0){
            dm=rateDM/count;
        }
        return baseRate*dm;
    }
    public static boolean isSucess(double rate){
        Random random=new Random();
        int seed=0;
        if(rate>0 && rate<1){
            BigDecimal bigDecimal=new BigDecimal(rate+"");
            BigDecimal divisor = BigDecimal.ONE;
            MathContext mc = new MathContext(1);
            bigDecimal=bigDecimal.divide(divisor, mc);
            int position = bigDecimal.scale();
            double temp=Math.pow(10,position);
            rate*=temp;
            seed=(int)(100*temp);
        }else if(rate>=1){
            seed=100;
        }else if(rate<=0){
            return false;
        }
        int ranNum=random.nextInt(seed);
        return ranNum < rate;
    }

    public static void main(String[] args) {
        isSucess(calRate(100.0,1,20));
    }
}
