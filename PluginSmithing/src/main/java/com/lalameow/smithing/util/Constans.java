package com.lalameow.smithing.util;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/4
 * 时间: 2:07
 * 功能：请进行修改
 */
public class Constans {
    public final static String NBTCUSTOMNAME="CustomName";
    public final static String NBTITEMS="Items";
    public final static String NBTSLOT="ISlot";
    public final static String NBTID="id";
    public final static String NBTCOOKTIME="CookTime";
    public final static String NBTTAG="tag";
    public final static String NBTDISPLAY="display";
    public final static String NBTNAME="Name";
    public final static String NBTHOLE="hole";
    public final static String NBTSTRENGTHEN="strengthen";
    public final static String NBTUPGRADE="upgrade";
    public final static String NBTLORE="Lore";

}
