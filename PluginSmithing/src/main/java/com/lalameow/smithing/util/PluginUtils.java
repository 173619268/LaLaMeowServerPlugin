package com.lalameow.smithing.util;

import com.google.gson.Gson;
import com.lalameow.smithing.Smithing;
import com.lalameow.smithing.config.ConfigManager;
import com.lalameow.smithing.entity.EquimentEntity;
import com.lalameow.smithing.entity.FuelItemEntity;
import com.lalameow.smithing.enumeration.EquimentType;
import com.lalameow.smithing.enumeration.FuelOperType;
import me.dpohvar.powernbt.api.NBTCompound;
import me.dpohvar.powernbt.api.NBTManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/4
 * 时间: 3:40
 * 功能：请进行修改
 */
public class PluginUtils {
    /**
     * 类型转换
     * @param type
     * @param value
     * @return
     */
    public static Object getType(String type,String value){
        if(type.equalsIgnoreCase("byte")){
            return Byte.parseByte(value);
        }else if(type.equalsIgnoreCase("short")){
            return Short.parseShort(value);
        }else if(type.equalsIgnoreCase("int")){
            return Integer.parseInt(value);
        }else if(type.equalsIgnoreCase("long")){
            return Long.parseLong(value);
        }else if(type.equalsIgnoreCase("float")){
            return Float.parseFloat(value);
        }else if(type.equalsIgnoreCase("double")){
            return Double.parseDouble(value);
        }else if(type.equalsIgnoreCase("string")){
            return value;
        }else if(type.equalsIgnoreCase("byte[]")){
            Gson gson=new Gson();
            return gson.fromJson(value,byte[].class);
        }else if(type.equalsIgnoreCase("list")){
            Gson gson=new Gson();
            return gson.fromJson(value,List.class);
        }else if(type.equalsIgnoreCase("int[]")){
            Gson gson=new Gson();
            return gson.fromJson(value,int[].class);
        }else {
            return null;
        }
    }

    /**
     * 转化颜色代码
     * @param msg
     * @return
     */
    public static String translateColorSign(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }
    public static List<String> translateColorSignList(List msgList) {
        List<String> transList=new ArrayList<String>();
        for (Object aMsgList : msgList) {
            if(aMsgList instanceof String){
                transList.add(ChatColor.translateAlternateColorCodes('&', (String)aMsgList));
            }

        }
        return transList;
    }

    /**
     * 更新装备的锻造信息
     * @param forgeItem
     * @param equimentEntity
     * @param fuelItemEntity
     * @return
     */
    public static ItemStack updateDzLoreInfo(ItemStack forgeItem, EquimentEntity equimentEntity, FuelItemEntity fuelItemEntity){
        NBTCompound forgeItemNBT= NBTManager.nbtManager.read(forgeItem);
        ItemMeta itemMeta=forgeItem.getItemMeta();
        List<String> itemLore=itemMeta.getLore();
        if(itemLore==null){
            itemLore=new ArrayList<String>();
        }
        //存储已有的宝石属性
        List<String> inlyLore=new ArrayList<String>();
        String holelable= ConfigManager.mainConfig.getHolelable();
        String holeemlable= ConfigManager.mainConfig.getHoleemlable();
        String qhsxLore="";
        for (String itemlorestr : itemLore) {
            if(itemlorestr.contains(holelable)&&!itemlorestr.contains(holeemlable)){
                inlyLore.add(itemlorestr);
            }
            if(itemlorestr.contains("强化属性")){
                qhsxLore=itemlorestr;
            }
        }

        //清除已有的锻造信息
        Iterator<String> loreIter=itemLore.iterator();
        boolean removflag=false;
        while(loreIter.hasNext()){
            String lore = loreIter.next();
            if(lore.contains(ConfigManager.mainConfig.getDzjslore())){
                loreIter.remove();
                removflag=false;
            }
            if(lore.contains(ConfigManager.mainConfig.getDzkslore())){
                loreIter.remove();
                removflag=true;
            }else if(removflag){
                loreIter.remove();
            }
        }

        //添加锻造信息lore
        List<String> dzLoreInfo=new ArrayList<String>();
        dzLoreInfo.add(ConfigManager.mainConfig.getDzkslore());
        if(equimentEntity.isCanStrengthen()){//添加强化信息
            MessageFormat qhcsformat=new MessageFormat(ConfigManager.mainConfig.getQhcslore());
            MessageFormat qhsxformat=new MessageFormat(ConfigManager.mainConfig.getQhsxlore());
            String qhAttr="";
            if(equimentEntity.getEqType().equals(EquimentType.WEAPON)){
                qhAttr= ConfigManager.mainConfig.getWeaponlable();
            }else if(equimentEntity.getEqType().equals(EquimentType.ARMOR)){
                qhAttr= ConfigManager.mainConfig.getArmorlable();
            }
            int strengthenCount=forgeItemNBT.getInt(Constans.NBTSTRENGTHEN);
            //添加强化信息
            dzLoreInfo.add(qhcsformat.format(new Object[]{strengthenCount,equimentEntity.getMaxStrengthen()}));
            if(fuelItemEntity!=null && fuelItemEntity.getOperType()== FuelOperType.STRENGTHEN){
                //计算强化属性
                Integer base=Integer.parseInt(fuelItemEntity.getNBTLable());
                Double multiplier=Double.parseDouble(fuelItemEntity.getNBTValue());
                Integer attrValue=(int)(strengthenCount*base*multiplier);
                //添加强化属性
                dzLoreInfo.add(qhsxformat.format(new Object[]{attrValue,qhAttr}));
            }else if(!qhsxLore.equals("")){
                dzLoreInfo.add(qhsxLore);
            }
        }
        if(equimentEntity.isCanUpgrade()){//添加升级信息
            int upcradeCount=forgeItemNBT.getInt(Constans.NBTUPGRADE);
            MessageFormat dzclformat=new MessageFormat(ConfigManager.mainConfig.getDzcllore());
            dzLoreInfo.add(dzclformat.format(new Object[]{equimentEntity.getUpgradeFuel()}));//添加锻造材料的描述
            if(upcradeCount>=equimentEntity.getUpgradeCount()){
                MessageFormat dzsjformat=new MessageFormat(ConfigManager.mainConfig.getDzsjlore());
                //添加进化信息
                dzLoreInfo.add(dzsjformat.format(new Object[]{ChatColor.GOLD+equimentEntity.getUpgradeItemName()}));
            }else{
                MessageFormat dzcsformat=new MessageFormat(ConfigManager.mainConfig.getDzcslore());
                //添加升级信息
                dzLoreInfo.add(dzcsformat.format(new Object[]{upcradeCount, equimentEntity.getUpgradeCount()}));
            }

        }
        if(equimentEntity.isCanHole()){//添加宝石和镶嵌信息
            MessageFormat dlslformat=new MessageFormat(ConfigManager.mainConfig.getDksllore());
            int curenHole=forgeItemNBT.getInt(Constans.NBTHOLE);
            int maxHole=equimentEntity.getMaxhole();
            dzLoreInfo.add(dlslformat.format(new Object[]{curenHole,maxHole}));//添加打孔信息
            dzLoreInfo.addAll(inlyLore);//添加已经镶嵌的lore
            if(curenHole-inlyLore.size()>0){
                MessageFormat holeformat=new MessageFormat(ConfigManager.mainConfig.getHolelore());
                String holrfromatlore=holeformat.format(new Object[]{holelable,holeemlable});
                for(int i=0;i<curenHole-inlyLore.size();i++){
                    dzLoreInfo.add(holrfromatlore);//添加空槽lore
                }
            }
        }
        dzLoreInfo.add(ConfigManager.mainConfig.getDzjslore());
        itemLore.addAll(dzLoreInfo);
        itemMeta.setLore(itemLore);
        forgeItem.setItemMeta(itemMeta);
        return  forgeItem;
    }



    /**
     * 全服务器公告
     * @param msg
     */
    public static void broadcast(String msg){
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(PluginUtils.translateColorSign(msg));
        }
    }

    /**
     * 获取装备的空槽
     * @param lorelist
     * @return
     */
    public static int getEmptyHole(List<String> lorelist){
        String holelable= ConfigManager.mainConfig.getHolelable();
        String holeemlable= ConfigManager.mainConfig.getHoleemlable();
        int emptyHole=0;
        for (String itemLore : lorelist) {
            if(itemLore.contains(holelable)&&itemLore.contains(holeemlable)){
                emptyHole++;
            }
        }
        return emptyHole;
    }

    /**
     * 获取装备已镶嵌宝石数量
     * @param lorelist
     * @return
     */
    public static int getNoEmptyHole(List<String> lorelist){
        String holelable= ConfigManager.mainConfig.getHolelable();
        String holeemlable= ConfigManager.mainConfig.getHoleemlable();
        int noemptyHole=0;
        for (String itemLore : lorelist) {
            if(itemLore.contains(holelable)&&!itemLore.contains(holeemlable)){
                noemptyHole++;
            }
        }
        return noemptyHole;
    }
}
