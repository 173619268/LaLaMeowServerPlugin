package com.lalameow.smithing.util;

import com.lalameow.common.exception.LaLaMeowException;
import com.lalameow.smithing.config.ConfigManager;
import com.lalameow.smithing.entity.EquimentEntity;
import com.lalameow.smithing.entity.FuelItemEntity;
import com.lalameow.smithing.enumeration.EquimentType;
import com.lalameow.smithing.enumeration.FuelOperType;
import me.dpohvar.powernbt.api.NBTCompound;
import me.dpohvar.powernbt.api.NBTList;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/9
 * 时间: 1:03
 * 功能：请进行修改
 */
public class VaildUtils {

    /**
     * 锻造炉验证
     * @param equimentEntity
     * @param fuel
     * @param foreItemNBT
     * @return
     * @throws LaLaMeowException
     */
    public static boolean vailEqmentOper(EquimentEntity equimentEntity, ItemStack fuel, NBTCompound foreItemNBT) throws LaLaMeowException {
        FuelItemEntity fuelItemEntity= ConfigManager.fuelItemMap.get(ChatColor.stripColor(fuel.getItemMeta().getDisplayName()));
        if(equimentEntity.isCanUpgrade() && fuelItemEntity.getOperType()== FuelOperType.UPGRADE) {
            vaildUpgrade(equimentEntity, fuel);
            return true;
        }else if(equimentEntity.isCanHole() && fuelItemEntity.getOperType()== FuelOperType.ADDHOLE){
            vailHole(equimentEntity, foreItemNBT);
            return true;
        }else if(equimentEntity.isCanStrengthen() && fuelItemEntity.getOperType()== FuelOperType.STRENGTHEN){
            vailStrengthen(equimentEntity, foreItemNBT,fuelItemEntity);
            return true;
        }else if(fuelItemEntity.getOperType()== FuelOperType.ADDNBT || fuelItemEntity.getOperType()== FuelOperType.DELNBT){
            return true;
        }else if(fuelItemEntity.getOperType()== FuelOperType.INLAY){
            vailInlay(equimentEntity, foreItemNBT,fuelItemEntity);
            return true;
        }
        return false;
    }

    /**
     * 镶嵌校验
     * @param equimentEntity
     * @param foreItemNBT
     * @param fuelItemEntity
     * @throws LaLaMeowException
     */
    public static void vailInlay(EquimentEntity equimentEntity, NBTCompound foreItemNBT, FuelItemEntity fuelItemEntity) throws LaLaMeowException {

        if(equimentEntity.getEqType()!= EquimentType.getByTypeName(fuelItemEntity.getNBTType())){
            throw new LaLaMeowException("宝石类型不能镶嵌此装备！");
        }
        List<String> lorelist=readItemLore(foreItemNBT);
        if(lorelist==null){
            throw new LaLaMeowException("此装备不支持宝石镶嵌！");
        }
        if(lorelist.isEmpty()){
            throw new LaLaMeowException("此装备没有宝石槽位无法镶嵌！");
        }
        if(PluginUtils.getEmptyHole(lorelist)<=0){
            throw new LaLaMeowException("装备没有空余的宝石插槽，无法镶嵌！");
        }
    }
    /**
     * 验证是否可以强化
     * @param equimentEntity
     * @param foreItemNBT
     * @param fuelItemEntity
     * @throws LaLaMeowException
     */
    public static void vailStrengthen(EquimentEntity equimentEntity, NBTCompound foreItemNBT, FuelItemEntity fuelItemEntity) throws LaLaMeowException {
        EquimentType equimentType= EquimentType.getByTypeName(fuelItemEntity.getNBTType());
        if(equimentType==null){
            throw new LaLaMeowException("强化石类型错误！");
        }
        if(equimentType!=equimentEntity.getEqType()){
            throw new LaLaMeowException(equimentType.getTypeName()+"强化石无法强化"+equimentEntity.getEqType().getTypeName()+"！");
        }
        if(equimentEntity.getMaxStrengthen()<=0){
            throw new LaLaMeowException("装备设置的最大强化等级异常");
        }
        if(getStrengthenCount(foreItemNBT)>=equimentEntity.getMaxStrengthen()){
            throw new LaLaMeowException("装备已经达到最大强化等级，无法进行强化！");
        }

    }

    /**
     * 验证是可打孔
     * @param equimentEntity
     * @param foreItemNBT
     * @throws LaLaMeowException
     */
    public static void vailHole(EquimentEntity equimentEntity, NBTCompound foreItemNBT) throws LaLaMeowException {
        if(equimentEntity.getMaxhole()<=0){
            throw new LaLaMeowException("装备设置的最大孔异常！");
        }
        if(getHoleCount(foreItemNBT)>=equimentEntity.getMaxhole()){
            throw new LaLaMeowException("装备无法继续开孔！");
        }

    }
    /**
     * 验证配置文件升级的必备参数
     * @param equimentEntity
     * @param fuelItem
     * @throws LaLaMeowException
     */
    public static void vaildUpgrade(EquimentEntity equimentEntity, ItemStack fuelItem) throws LaLaMeowException {
        if(equimentEntity.getUpgradeCount()<=0){
            throw new LaLaMeowException("装备升级所需的锻造石数量定义错误！");
        }
        FuelItemEntity fuelItemEntity= ConfigManager.fuelItemMap.get(equimentEntity.getUpgradeFuel());
        if(fuelItemEntity==null){
            throw new LaLaMeowException("装备使用的锻造石不能存在！");
        }
        if(!fuelItemEntity.getFuelItemTranName().equals(fuelItem.getItemMeta().getDisplayName())){
            throw new LaLaMeowException("此装备只能用"+ ChatColor.GOLD+"["+equimentEntity.getUpgradeFuel()+"]"+ ChatColor.RED+"锻造石进行升级");
        }
        if(!ConfigManager.equimentMap.keySet().contains(equimentEntity.getUpgradeItemName())){
            throw new LaLaMeowException("此装备进阶的装备名不能存在！");
        }
    }

    /**
     * 验证是否是锻造炉
     * @param blockName
     * @return
     */
    public static boolean vaildSmithBlock(String blockName){
        return ConfigManager.smithBlockConfig.getSmithBlockName().equals(blockName);
    }

    /**
     * 验证装备是否在装备库中
     * @param eqNBT
     * @return
     * @throws LaLaMeowException
     */
    public static  boolean vaildEqument(NBTCompound eqNBT) throws LaLaMeowException {
        String name=readItemName(eqNBT);
        if(name==null){
            return false;
        }
        for (Map.Entry<String, EquimentEntity> stringEquimentEntityEntry :  ConfigManager.equimentMap.entrySet()) {
            if(stringEquimentEntityEntry.getValue().getEqumentTranName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }
    public static  boolean vaildEqument(String eqName) throws LaLaMeowException {
        if(eqName==null){
            return false;
        }
        for (Map.Entry<String, EquimentEntity> stringEquimentEntityEntry :  ConfigManager.equimentMap.entrySet()) {
            if(stringEquimentEntityEntry.getValue().getEqumentTranName().equalsIgnoreCase(eqName)){
                return true;
            }
        }
        return false;
    }

    /**
     * 验证锻造石是否在库中
     * @param itemNBT
     * @return
     * @throws LaLaMeowException
     */
    public static  boolean vaildFuel(NBTCompound itemNBT) throws LaLaMeowException {
        String name=readItemName(itemNBT);
        if(name==null){
            return false;
        }
        for (Map.Entry<String, FuelItemEntity> stringFuelItemEntityEntry :  ConfigManager.fuelItemMap.entrySet()) {
            if(stringFuelItemEntityEntry.getValue().getFuelItemTranName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }
    public static  boolean vaildFuel(ItemStack itemName) throws LaLaMeowException {
        if(itemName==null){
            return false;
        }
        if(itemName.getItemMeta()==null){
            return false;
        }
        if(itemName.getItemMeta().getDisplayName()==null){
            return false;
        }
        for (Map.Entry<String, FuelItemEntity> stringFuelItemEntityEntry : ConfigManager.fuelItemMap.entrySet()) {
            if(stringFuelItemEntityEntry.getValue().getFuelItemTranName().equalsIgnoreCase(itemName.getItemMeta().getDisplayName())){
                return true;
            }
        }
        return false;
    }

    /**
     * NBT节点读取文件名称
     * @param itemNBT
     * @return
     */
    public static String readItemName(NBTCompound itemNBT){
        NBTCompound tagCompound=itemNBT.getCompound(Constans.NBTTAG);
        if(tagCompound==null){
            return null;
        }
        NBTCompound diplayCompound=tagCompound.getCompound(Constans.NBTDISPLAY);
        if(diplayCompound==null){
            return null;
        }
        return diplayCompound.getString(Constans.NBTNAME);
    }

    /**
     * 读取NBT节点的lore列表
     * @param itemNBT
     * @return
     */
    public static List<String> readItemLore(NBTCompound itemNBT){
        NBTCompound tagCompound=itemNBT.getCompound(Constans.NBTTAG);
        if(tagCompound==null){
            return null;
        }
        NBTCompound diplayCompound=tagCompound.getCompound(Constans.NBTDISPLAY);
        if(diplayCompound==null){
            return null;
        }
        NBTList loreList=diplayCompound.getList(Constans.NBTLORE);
        if(loreList==null){
            return null;
        }
        List<String> lores=new ArrayList<String>();
        for (Object o : loreList) {
            lores.add((String)o);
        }
        return lores;
    }
    /**
     * NBT节点装备宝石孔(只适用于容器)
     * @param itemNBT
     * @return
     */
    public static Integer  getHoleCount(NBTCompound itemNBT){
        NBTCompound tagCompound=itemNBT.getCompound(Constans.NBTTAG);
        if(tagCompound==null){
            return 0;
        }
        return tagCompound.getInt(Constans.NBTHOLE);
    }

    /**
     * 获取装备强化等级(只适用于容器)
     * @param itemNBT
     * @return
     */
    public static Integer getStrengthenCount(NBTCompound itemNBT){
        NBTCompound tagCompound=itemNBT.getCompound(Constans.NBTTAG);
        if(tagCompound==null){
            return 0;
        }
        return tagCompound.getInt(Constans.NBTSTRENGTHEN);
    }
}
