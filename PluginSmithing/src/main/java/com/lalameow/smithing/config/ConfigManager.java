package com.lalameow.smithing.config;

import com.lalameow.smithing.entity.EquimentEntity;
import com.lalameow.smithing.entity.FuelItemEntity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/2/28
 * 时间: 18:11
 * 功能：请进行修改
 */
public class ConfigManager {
    public static EquipmentConfig equipmentConfig;
    public static SmithBlockConfig smithBlockConfig;
    public static FuelItemConfig fuelItemConfig;
    public static MainConfig mainConfig;

    public static HashMap<String,EquimentEntity> equimentMap=new HashMap<String, EquimentEntity>();
    public static HashMap<String,FuelItemEntity> fuelItemMap=new HashMap<String, FuelItemEntity>();

    public static Set<Integer> equimentIdSet=new HashSet<Integer>();
    public static Set<Integer> fuelItemIdSet=new HashSet<Integer>();
    public static void init(){
       mainConfig=new MainConfig();
       smithBlockConfig=new SmithBlockConfig();
       fuelItemConfig=new FuelItemConfig();
       equipmentConfig=new EquipmentConfig();
    }

}
