package com.lalameow.smithing.config;

import com.google.gson.Gson;
import com.lalameow.common.config.AbstractConfig;
import com.lalameow.common.exception.LaLaMeowException;
import com.lalameow.smithing.Smithing;
import com.lalameow.smithing.entity.FuelItemEntity;
import com.lalameow.smithing.enumeration.EquimentType;
import com.lalameow.smithing.enumeration.FuelOperType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/4
 * 时间: 1:03
 * 功能：请进行修改
 */
public class FuelItemConfig extends AbstractConfig {

    public final static String ID="id";
    public final static String CUSNAME="cusname";
    public final static String OPER="oper";
    public final static String NBTLABLE="nbtlable";
    public final static String TYPE="type";
    public final static String VALUE="value";
    public final static String RATE="rate";
    public final static String LORE="lore";



     FuelItemConfig() {
        super("fuelitem.yml",FuelItemEntity.class,Smithing.plugin);
    }

    public ItemStack getFuleItem(String fuelItemName) throws LaLaMeowException {
        if(!ConfigManager.fuelItemMap.keySet().contains(fuelItemName)){
            throw new LaLaMeowException("获取的锻造石头不存在");
        }
        FuelItemEntity fuelItemEntity=ConfigManager.fuelItemMap.get(fuelItemName);
        ItemStack itemStack=new ItemStack(Material.getMaterial(fuelItemEntity.getId()));
        ItemMeta itemMeta=itemStack.getItemMeta();
        itemMeta.setDisplayName(fuelItemEntity.getCusname());
        itemMeta.setLore(fuelItemEntity.getLore());
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    @Override
    public void createDefault() {
        Smithing.plugin.getLogger().info("正在创建锻造石默认配置文件");
        ConfigManager.fuelItemMap.clear();
        FuelItemEntity fuelNBTAddTest=createNBTAdd();
        FuelItemEntity fuelNBTRemoveTest=createNBTRemove();
        FuelItemEntity fuelStrengthenTest=createSthrenthen();
        FuelItemEntity fuelAddHoleTest=createHole();
        FuelItemEntity fuleUpgrade=createUpgrade();
        FuelItemEntity fuleInlay=createInlay();
        try {
            ConfigManager.fuelItemMap.put(fuelNBTAddTest.getFuelItemName(), fuelNBTAddTest);
            ConfigManager.fuelItemMap.put(fuelNBTRemoveTest.getFuelItemName(), fuelNBTRemoveTest);
            ConfigManager.fuelItemMap.put(fuelStrengthenTest.getFuelItemName(), fuelStrengthenTest);
            ConfigManager.fuelItemMap.put(fuelAddHoleTest.getFuelItemName(), fuelAddHoleTest);
            ConfigManager.fuelItemMap.put(fuleUpgrade.getFuelItemName(), fuleUpgrade);
            ConfigManager.fuelItemMap.put(fuleInlay.getFuelItemName(), fuleInlay);
            Map<String,Object> defaultsMap=new HashMap<String, Object>();
            defaultsMap.putAll(ConfigManager.fuelItemMap);
            this.getConfigFile().options().copyDefaults(true);
            this.getConfigFile().addDefaults(defaultsMap);
            this.save();
        } catch (LaLaMeowException e) {
            Smithing.plugin.getLogger().info("锻造石配置文件创建失败，不存在的锻造石名称");
        }
        Smithing.plugin.getLogger().info("默认文件创建完毕");
    }

    @Override
    public void initConfig() {
        Smithing.plugin.getLogger().info("读取锻造石配置文件");
        ConfigManager.fuelItemMap.clear();
        Gson gson=new Gson();
        for (String fuleName : this.getConfigFile().getKeys(false)) {
            String jsonStr=gson.toJson(this.getConfigFile().getConfigurationSection(fuleName).getValues(true));
            FuelItemEntity fuelItemEntity=gson.fromJson(jsonStr,FuelItemEntity.class);
            ConfigManager.fuelItemMap.put(fuleName, fuelItemEntity);
        }
        Smithing.plugin.getLogger().info("读取锻造石完毕，一共添加了"+ConfigManager.fuelItemMap.size()+"个锻造石");
    }

    private FuelItemEntity createNBTAdd(){
        FuelItemEntity fuelNBTAddTest=new FuelItemEntity();
        fuelNBTAddTest.setCusname("&2NBT添加测试物品");
        fuelNBTAddTest.setId(264);
        fuelNBTAddTest.setOperType(FuelOperType.ADDNBT);
        fuelNBTAddTest.setNBTLable("Unbreakable");
        fuelNBTAddTest.setNBTType("byte");
        fuelNBTAddTest.setNBTValue("1");
        fuelNBTAddTest.getLore().add("&2这是一个测试描述");
        fuelNBTAddTest.getLore().add("&b&l这是一个测试描述");
        return fuelNBTAddTest;
    }
    private FuelItemEntity createNBTRemove(){
        FuelItemEntity fuelNBTRemoveTest=new FuelItemEntity();
        fuelNBTRemoveTest.setCusname("&2NBT删除测试物品");
        fuelNBTRemoveTest.setId(388);
        fuelNBTRemoveTest.setOperType(FuelOperType.DELNBT);
        fuelNBTRemoveTest.setNBTLable("AMArmorProperties");
        return fuelNBTRemoveTest;
    }
    private FuelItemEntity createSthrenthen(){
        FuelItemEntity fuelNBTRemoveTest=new FuelItemEntity();
        fuelNBTRemoveTest.setCusname("&4强化测试物品");
        fuelNBTRemoveTest.setId(388);
        fuelNBTRemoveTest.setOperType(FuelOperType.STRENGTHEN);
        fuelNBTRemoveTest.setNBTLable("20");
        fuelNBTRemoveTest.setNBTType(EquimentType.WEAPON.getTypeName());
        fuelNBTRemoveTest.setNBTValue("1");
        return fuelNBTRemoveTest;
    }
    private FuelItemEntity createUpgrade(){
        FuelItemEntity fuelNBTRemoveTest=new FuelItemEntity();
        fuelNBTRemoveTest.setCusname("&4血红精金");
        fuelNBTRemoveTest.setId(388);
        fuelNBTRemoveTest.setOperType(FuelOperType.UPGRADE);
        return fuelNBTRemoveTest;
    }
    private FuelItemEntity createHole(){
        FuelItemEntity fuelNBTRemoveTest=new FuelItemEntity();
        fuelNBTRemoveTest.setCusname("&4打孔物品测试");
        fuelNBTRemoveTest.setId(388);
        fuelNBTRemoveTest.setOperType(FuelOperType.ADDHOLE);
        return fuelNBTRemoveTest;
    }
    private FuelItemEntity createInlay(){
        FuelItemEntity fuelNBTRemoveTest=new FuelItemEntity();
        fuelNBTRemoveTest.setCusname("&4镶嵌物品测试");
        fuelNBTRemoveTest.setId(388);
        fuelNBTRemoveTest.setOperType(FuelOperType.INLAY);
        fuelNBTRemoveTest.setNBTLable("伤害");
        fuelNBTRemoveTest.setNBTType(EquimentType.WEAPON.getTypeName());
        fuelNBTRemoveTest.setNBTValue("20");
        return fuelNBTRemoveTest;
    }
}
