package com.lalameow.smithing.config;

import com.lalameow.common.config.AbstractConfig;
import com.lalameow.smithing.Smithing;
import com.lalameow.smithing.util.PluginUtils;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/4
 * 时间: 1:03
 * 功能：请进行修改
 */
public class SmithBlockConfig extends AbstractConfig {
    SmithBlockConfig() {
        super("smithblock.yml",null,Smithing.plugin);
    }

    public String getSmithBlockName() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString("smithblock.cusname"));
    }

    public List<String> getAppfuelList() {
        return this.getConfigFile().getStringList("smithblock.appfuel");
    }

    public List<String> getLoreList() {
        return PluginUtils.translateColorSignList(this.getConfigFile().getStringList("smithblock.lore"));
    }

    /**
     * 获取熔炉
     * @return
     */
    public ItemStack getSmithItem(){
        ItemStack itemStack=new ItemStack(Material.FURNACE);
        ItemMeta itemMeta=itemStack.getItemMeta();
        itemMeta.setDisplayName(getSmithBlockName());
        itemMeta.setLore(getLoreList());
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    @Override
    public void createDefault() {
        Smithing.plugin.getLogger().info("锻造炉配置文件创建");
        try {
            this.getConfigFile().load(new InputStreamReader(Smithing.plugin.getResource(this.getFileName()), "UTF-8"));
            this.save();
        } catch (IOException e) {
            e.printStackTrace();
            Smithing.plugin.getLogger().info("锻造炉配置文件锻造失败");
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            Smithing.plugin.getLogger().info("锻造炉配置文件锻造失败");
        }
        Smithing.plugin.getLogger().info("锻造炉配置文件创造完毕");
    }

    @Override
    public void initConfig() {

    }
}
