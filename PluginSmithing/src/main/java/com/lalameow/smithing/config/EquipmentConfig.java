package com.lalameow.smithing.config;

import com.google.gson.Gson;
import com.lalameow.common.config.AbstractConfig;
import com.lalameow.common.exception.LaLaMeowException;
import com.lalameow.smithing.Smithing;
import com.lalameow.smithing.entity.EquimentEntity;
import com.lalameow.smithing.enumeration.EquimentType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/7
 * 时间: 18:13
 * 功能：请进行修改
 */
public class EquipmentConfig extends AbstractConfig {
    protected EquipmentConfig() {
        super("equipment.yml", EquimentEntity.class,Smithing.plugin);
    }

    @Override
    public void createDefault() {
        Smithing.plugin.getLogger().info("创建装备库默认配置文件");
        ConfigManager.equimentMap.clear();
        EquimentEntity equimentEntity=createEq();
        try {
            ConfigManager.equimentMap.put(equimentEntity.getEqumentName(), equimentEntity);
            Map<String,Object> defaultsMap=new HashMap<String, Object>();
            defaultsMap.putAll(ConfigManager.equimentMap);
            this.getConfigFile().options().copyDefaults(true);
            this.getConfigFile().addDefaults(defaultsMap);
            this.save();
        } catch (LaLaMeowException e) {
            Smithing.plugin.getLogger().info("装备库默认文件创建失败，不存在的锻造装备名称");
        }
        Smithing.plugin.getLogger().info("装备库默认文件创建完毕");
    }

    @Override
    public void initConfig() {
        Smithing.plugin.getLogger().info("读取锻造装备配置文件");
        ConfigManager.equimentMap.clear();
        Gson gson=new Gson();
        for (String fuleName : this.getConfigFile().getKeys(false)) {
            String jsonStr=gson.toJson(this.getConfigFile().getConfigurationSection(fuleName).getValues(true));
            EquimentEntity equimentEntity=gson.fromJson(jsonStr,EquimentEntity.class);
            ConfigManager.equimentMap.put(fuleName, equimentEntity);
        }
        Smithing.plugin.getLogger().info("读取装备完毕，一共添加了"+ConfigManager.fuelItemMap.size()+"个锻造装备");
    }

    private EquimentEntity createEq(){
        EquimentEntity equimentEntity=new EquimentEntity();
        equimentEntity.setCusname("&6测试武器");
        equimentEntity.setEqType(EquimentType.WEAPON);
        equimentEntity.setId(267);
        equimentEntity.getLore().add("&2+30 伤害");
        equimentEntity.getLore().add("&6测试武器毁天灭地");
        equimentEntity.getLore().add("&7你可以试试");
        return equimentEntity;
    }
}
