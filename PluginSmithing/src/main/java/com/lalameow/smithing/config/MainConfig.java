package com.lalameow.smithing.config;

import com.lalameow.common.config.AbstractConfig;
import com.lalameow.smithing.Smithing;
import com.lalameow.smithing.util.PluginUtils;
import org.bukkit.configuration.InvalidConfigurationException;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/9
 * 时间: 19:20
 * 功能：请进行修改
 */
public class MainConfig extends AbstractConfig {

    public static final String DZKSLORE = "dzkslore";
    public static final String QHCSLORE = "qhcslore";
    public static final String QHSXLORE = "qhsxlore";
    public static final String DZCSLORE = "dzcslore";
    public static final String DKSLLORE = "dksllore";
    public static final String DZJSLORE = "dzjslore";
    public static final String WEAPONLABLE = "weaponlable";
    public static final String ARMORLABLE = "armorlable";
    public static final String DZSJLORE = "dzsjlore";
    public static final String HOLELORE = "holelore";
    public static final String INLYLORE = "inlylore";
    public static final String HOLELABLE = "holelable";
    public static final String HOLEEMLABLE = "holeemlable";
    public static final String DZCLLORE = "dzcllore";
    public static final String KEEPLVNAME = "keeplvname";

    protected MainConfig() {
        super("config.yml", null, Smithing.plugin);
    }

    public String getDzcllore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(DZCLLORE));
    }

    /**
     * 保护符
     *
     * @return
     */
    public String getKeeplvname() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(KEEPLVNAME));
    }

    /**
     * 强化武器的标识符
     *
     * @return
     */
    public String getWeaponlable() {
        return this.getConfigFile().getString(WEAPONLABLE);
    }

    public String getArmorlable() {
        return this.getConfigFile().getString(ARMORLABLE);
    }

    /**
     * 宝石孔标志付
     *
     * @return
     */
    public String getHolelable() {
        return this.getConfigFile().getString(HOLELABLE);
    }

    /**
     * 空槽位标志付
     *
     * @return
     */
    public String getHoleemlable() {
        return this.getConfigFile().getString(HOLEEMLABLE);
    }

    /**
     * 宝石lore样式
     *
     * @return
     */
    public String getHolelore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(HOLELORE));
    }

    /**
     * 镶嵌lore样式
     *
     * @return
     */
    public String getInlylore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(INLYLORE));
    }

    /**
     * 锻造升级显示的lore
     *
     * @return
     */
    public String getDzsjlore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(DZSJLORE));
    }


    public String getDzkslore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(DZKSLORE));
    }

    public String getQhcslore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(QHCSLORE));
    }

    public String getQhsxlore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(QHSXLORE));
    }

    public String getDzcslore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(DZCSLORE));
    }

    public String getDksllore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(DKSLLORE));
    }

    public String getDzjslore() {
        return PluginUtils.translateColorSign(this.getConfigFile().getString(DZJSLORE));
    }

    @Override
    public void createDefault() {
        Smithing.plugin.getLogger().info("主配置文件创建");
        try {
            this.getConfigFile().load(new InputStreamReader(Smithing.plugin.getResource(this.getFileName()), "UTF-8"));
            this.save();
        } catch (IOException e) {
            e.printStackTrace();
            Smithing.plugin.getLogger().info("主配置文件锻造失败");
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            Smithing.plugin.getLogger().info("主配置文件锻造失败");
        }
        Smithing.plugin.getLogger().info("主配置文件创造完毕");
    }

    @Override
    public void initConfig() {

    }
}
