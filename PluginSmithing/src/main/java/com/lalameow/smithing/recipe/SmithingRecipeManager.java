package com.lalameow.smithing.recipe;

import com.lalameow.smithing.Smithing;
import com.lalameow.smithing.config.ConfigManager;
import com.lalameow.smithing.entity.EquimentEntity;
import com.lalameow.smithing.entity.FuelItemEntity;
import org.bukkit.Material;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/3
 * 时间: 13:46
 * 功能：请进行修改
 */
public class SmithingRecipeManager {

    public static void initDur(){
        int conut=0;
        for (Integer integer : ConfigManager.equimentIdSet) {
            Material material= Material.getMaterial(integer);
            try {
                if(material.getMaxDurability()!=1000){
                    Field durability = material.getClass().getDeclaredField("durability");
                    Field utfsystemUtfmodifiersField = Field.class.getDeclaredField("modifiers");
                    utfsystemUtfmodifiersField.setAccessible(true);
                    utfsystemUtfmodifiersField.setInt(durability, durability.getModifiers() & ~Modifier.FINAL);
                    durability.setAccessible(true);
                    durability.set(material, (short)1000);
                    conut++;
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
//        for (Material material : Material.values()) {
//            try {
//                if(material.getMaxDurability()<=0){
//                    Field durability = material.getClass().getDeclaredField("durability");
//                    Field utfsystemUtfmodifiersField = Field.class.getDeclaredField("modifiers");
//                    utfsystemUtfmodifiersField.setAccessible(true);
//                    utfsystemUtfmodifiersField.setInt(durability, durability.getModifiers() & ~Modifier.FINAL);
//                    durability.setAccessible(true);
//                    durability.set(material, (short)1000);
//                    conut++;
//                }
//            } catch (NoSuchFieldException e) {
//                e.printStackTrace();
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            }
//        }
        Smithing.plugin.getLogger().info("修改了"+conut+"个物品的最大耐久");
    }
    /**
     * 配方初始化
     */
    public static void init(){
        for (Map.Entry<String, EquimentEntity> stringEquimentEntityEntry : ConfigManager.equimentMap.entrySet()) {
            ConfigManager.equimentIdSet.add(stringEquimentEntityEntry.getValue().getId());
        }
        for (Map.Entry<String, FuelItemEntity> stringFuelItemEntityEntry : ConfigManager.fuelItemMap.entrySet()) {
            ConfigManager.fuelItemIdSet.add(stringFuelItemEntityEntry.getValue().getId());
        }
        Smithing.plugin.getLogger().info("一共注册的锻造装备"+ ConfigManager.equimentIdSet.size()+"个");
        Smithing.plugin.getLogger().info("一共注册的锻造石头"+ ConfigManager.fuelItemIdSet.size()+"个");
        ItemStack resultItem=new ItemStack(Material.COAL);
        ItemMeta meta=resultItem.getItemMeta();
        List<String> lores=new ArrayList<String>();
        lores.add("§c物品锻造失败形成的产物");
        lores.add("§c如果出现这个东西请联系管理员");
        meta.setLore(lores);
        resultItem.setItemMeta(meta);
        int count=0;
        for (Integer eqmentId : ConfigManager.equimentIdSet) {
            for (Integer fuelItemId : ConfigManager.fuelItemIdSet) {
                FurnaceRecipe recipe = new FurnaceRecipe(resultItem, Material.getMaterial(fuelItemId));
                recipe.setInput(Material.getMaterial(eqmentId));
                if(Smithing.plugin.getServer().addRecipe(recipe)){
                    count++;
                }
            }
        }
        Smithing.plugin.getLogger().info("成功添加了"+count+"个锻造配方");
    }

    /**
     * 添加锻造配方
     * @param id
     */
    public static void addRecipe(int id){
        ItemStack resultItem=new ItemStack(Material.COAL);
        ItemMeta meta=resultItem.getItemMeta();
        List<String> lores=new ArrayList<String>();
        lores.add("§c物品锻造失败形成的产物");
        meta.setLore(lores);
        resultItem.setItemMeta(meta);
        for (Integer fuelItemId : ConfigManager.fuelItemIdSet) {
            FurnaceRecipe recipe = new FurnaceRecipe(resultItem, Material.getMaterial(fuelItemId));
            recipe.setInput(Material.getMaterial(id));
            Smithing.plugin.getServer().addRecipe(recipe);
        }
    }



}
