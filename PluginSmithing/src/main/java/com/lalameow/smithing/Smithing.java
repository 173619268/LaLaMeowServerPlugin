package com.lalameow.smithing;

import com.lalameow.smithing.command.SmithingCommand;
import com.lalameow.smithing.config.ConfigManager;
import com.lalameow.smithing.listener.SmithingListener;
import com.lalameow.smithing.recipe.SmithingRecipeManager;
import me.dpohvar.powernbt.PowerNBT;
import me.dpohvar.powernbt.api.NBTManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/3
 * 时间: 12:57
 * 功能：请进行修改
 */
public class Smithing extends JavaPlugin {

    public static Smithing plugin;
    @Override
    public void onEnable() {
        plugin=this;
        ConfigManager.init();
        this.getLogger().info("PowerNBT插件加载完毕!");
        SmithingRecipeManager.init();
        this.getLogger().info("熔炉锻造配方初始化完毕!");
        SmithingRecipeManager.initDur();
        this.getLogger().info("锻造物品的耐久改变完毕!");
        this.getServer().getPluginManager().registerEvents(new SmithingListener(),this);
        this.getLogger().info("锻造工坊监听注册完毕!");
        getCommand("dzgf").setExecutor(new SmithingCommand());
        this.getLogger().info("LaLaMeow服务器专属插件-LaoChen!");

    }

    @Override
    public void onLoad() {

    }
    @Override
    public void onDisable() {
    }
}
