package com.lalameow.smithing.command;

import com.lalameow.common.command.AbstractCommand;
import com.lalameow.common.exception.LaLaMeowException;
import com.lalameow.common.message.LaLaMeowMessage;
import com.lalameow.smithing.config.ConfigManager;
import com.lalameow.smithing.entity.EquimentEntity;
import com.lalameow.smithing.entity.FuelItemEntity;
import com.lalameow.smithing.enumeration.EquimentType;
import com.lalameow.smithing.enumeration.FuelOperType;
import com.lalameow.smithing.recipe.SmithingRecipeManager;
import com.lalameow.smithing.util.PluginUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/3
 * 时间: 13:09
 * 功能：请进行修改
 */
public class SmithingCommand extends AbstractCommand {
    @Override
    protected void init() {
        command="dzgf";
        commands.put("addeqment [装备类型(武器|防具)]","添加装备到装备库");
        commands.put("addfuel [类型]","添加宝石到锻造石库。类型有(添加NBT,删除NBT,强化,升级,打孔,镶嵌)");
        commands.put("giveeqment 装备名称 [玩家名称]","给予玩家一个装备");
        commands.put("givefuel 锻造石名称 [玩家名称] [数量]","给予玩家一个锻造石");
        commands.put("giverl [玩家名称]","给予玩家一个锻造炉");
        commands.put("updateinfo","更新玩家手中物品的信息为库中的");
        commands.put("reload","重载配置文件");
    }

    protected void showHelp() {
        String msg=ChatColor.GREEN+"_____________["+ChatColor.RED+"锻造工坊]"+ChatColor.GREEN+"_______________\n";
        for (Map.Entry<String, String> stringStringEntry : commands.entrySet()) {
            msg+=ChatColor.RED+"[锻造工坊] "+ ChatColor.AQUA+"/"+command+" "+stringStringEntry.getKey()+"\n";
            msg+=ChatColor.RED+"[锻造工坊] "+ ChatColor.DARK_GREEN+stringStringEntry.getValue()+"\n";
        }
        msg+=ChatColor.GREEN+"_____________["+ChatColor.RED+"锻造工坊]"+ChatColor.GREEN+"_______________\n";
        this.sender.sendMessage(msg);
    }

    public void reload_cmd(){
        ConfigManager.mainConfig.reload();
        LaLaMeowMessage.sendSuccess(sender,"主配置文件重载完成！");
        ConfigManager.smithBlockConfig.reload();
        LaLaMeowMessage.sendSuccess(sender,"锻造炉配置文件重载完成！");
        ConfigManager.fuelItemConfig.reload();
        LaLaMeowMessage.sendSuccess(sender,"锻造石配置文件重载完成！");
        ConfigManager.equipmentConfig.reload();
        LaLaMeowMessage.sendSuccess(sender,"锻造装备重载完成！");
        SmithingRecipeManager.init();
        LaLaMeowMessage.sendSuccess(sender,"重载锻造配方！");
        SmithingRecipeManager.initDur();
        LaLaMeowMessage.sendSuccess(sender,"重载所有物品的耐久！");
    }
    public void updateinfo_cmd() throws LaLaMeowException {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            ItemStack itemStack = player.getItemInHand();
            if (itemStack == null) {
                throw new LaLaMeowException("你手里什么都没有");
            }
            ItemMeta itemMeta=itemStack.getItemMeta();
            if(itemMeta==null || itemMeta.getDisplayName()==null){
                throw new LaLaMeowException("此物品不是装备库中的物品");
            }
            EquimentEntity equimentEntity= ConfigManager.equimentMap.get(ChatColor.stripColor(itemMeta.getDisplayName()));
            if(!equimentEntity.getEqumentTranName().equals(itemMeta.getDisplayName())){
                throw new LaLaMeowException("此物品无效，请检查你的配置文件");
            }
            itemStack= PluginUtils.updateDzLoreInfo(itemStack,equimentEntity,null);
            player.setItemInHand(itemStack);
            LaLaMeowMessage.sendSuccess(sender,"更新装备锻造信息成功");
        }else {
            throw new LaLaMeowException("必须是玩家才可以执行此命令");
        }
    }
    public void giverl_cmd() throws LaLaMeowException {
        ConfigManager.smithBlockConfig.getSmithItem();
        if (sender instanceof Player) {
            ((Player) sender).getInventory().addItem(ConfigManager.smithBlockConfig.getSmithItem());
            LaLaMeowMessage.sendSuccess(sender, "成功获取一个熔炉！");
        } else {
            throw new LaLaMeowException("必须是玩家才可以执行此命令");
        }
    }
    public void addeqment_cmd() throws LaLaMeowException {
        if(this.args.length<2){
            throw new LaLaMeowException("参数个数不正确");
        }
        if (sender instanceof Player) {
            EquimentType equimentType= EquimentType.getByTypeName(args[1]);
            if(equimentType==null){
                throw new LaLaMeowException("装备类型错误！(武器|防具)");
            }
            EquimentEntity equimentEntity=new EquimentEntity();
            Player player=(Player) sender;
            ItemStack itemStack=player.getItemInHand();
            if(itemStack==null){
                throw new LaLaMeowException("你手里什么都没有");
            }
            ItemMeta itemMeta=itemStack.getItemMeta();
            equimentEntity.setCusname(itemMeta.getDisplayName());
            equimentEntity.setLore(itemMeta.getLore());
            equimentEntity.setId(itemStack.getType().getId());
            equimentEntity.setEqType(equimentType);
            ConfigManager.equimentMap.put(equimentEntity.getEqumentName(),equimentEntity);
            ConfigManager.equipmentConfig.getConfigFile().set(equimentEntity.getEqumentName(),equimentEntity);
            ConfigManager.equipmentConfig.save();
            LaLaMeowMessage.sendSuccess(sender,"成功添加装备到装备库中");
        } else {
            throw new LaLaMeowException("必须是玩家才可以执行此命令");
        }
    }
    public void addfuel_cmd() throws LaLaMeowException {
        if(this.args.length<2){
            throw new LaLaMeowException("参数个数不正确");
        }
        if (sender instanceof Player) {
            FuelOperType fuelOperType= FuelOperType.getByName(args[1]);
            if(fuelOperType==null){
                throw new LaLaMeowException("锻造是类型错误！(添加NBT,删除NBT,强化,升级,打孔,镶嵌)");
            }
            FuelItemEntity fuelItemEntity=new FuelItemEntity();
            Player player=(Player) sender;
            ItemStack itemStack=player.getItemInHand();
            if(itemStack==null){
                throw new LaLaMeowException("你手里什么都没有");
            }
            ItemMeta itemMeta=itemStack.getItemMeta();
            fuelItemEntity.setCusname(itemMeta.getDisplayName());
            fuelItemEntity.setLore(itemMeta.getLore());
            fuelItemEntity.setId(itemStack.getType().getId());
            fuelItemEntity.setOperType(fuelOperType);
            ConfigManager.fuelItemMap.put(fuelItemEntity.getFuelItemName(),fuelItemEntity);
            ConfigManager.fuelItemConfig.getConfigFile().set(fuelItemEntity.getFuelItemName(),fuelItemEntity);
            ConfigManager.fuelItemConfig.save();
            LaLaMeowMessage.sendSuccess(sender,"成功添加锻造石到库中");
        } else {
            throw new LaLaMeowException("必须是玩家才可以执行此命令");
        }
    }

    public void giveeqment_cmd() throws LaLaMeowException {
        Player player=null;
        if(args.length<2){
            throw new LaLaMeowException("参数个数不正确！");
        }else if(args.length<=2){
            if (sender instanceof Player) {
                player=(Player)sender;
            } else {
                throw new LaLaMeowException("必须是玩家才可以执行此命令");
            }
        }else if(args.length<=3){
             player= Bukkit.getPlayer(args[2]);
        }
        if(player==null){
            throw new LaLaMeowException("玩家不存在！");
        }
        String eqName=args[1];
        EquimentEntity equimentEntity= ConfigManager.equimentMap.get(eqName);
        if(equimentEntity==null){
            throw new LaLaMeowException("装备不存在与装备库！");
        }
        ItemStack itemStack=new ItemStack(Material.getMaterial(equimentEntity.getId()));
        ItemMeta itemMeta=itemStack.getItemMeta();
        itemMeta.setDisplayName(equimentEntity.getEqumentTranName());
        itemMeta.setLore(PluginUtils.translateColorSignList(equimentEntity.getLore()));
        itemStack.setItemMeta(itemMeta);
        itemStack= PluginUtils.updateDzLoreInfo(itemStack,equimentEntity,null);
        player.getInventory().addItem(itemStack);
        LaLaMeowMessage.sendSuccess(sender,"装备成功给予！");
    }

    public void givefuel_cmd() throws LaLaMeowException {
        Player player=null;
        int count=1;
        if(args.length<2){
            throw new LaLaMeowException("参数个数不正确！");
        }
        if(args.length==2){
            if (sender instanceof Player) {
                player=(Player)sender;
            } else {
                throw new LaLaMeowException("必须是玩家才可以执行此命令");
            }
        }
        if(args.length==3){
            player= Bukkit.getPlayer(args[2]);
        }
        if(args.length==4){
            player= Bukkit.getPlayer(args[2]);
            count=Integer.parseInt(args[3]);
        }
        if(player==null){
            throw new LaLaMeowException("玩家不存在！");
        }
        String fuelName=args[1];
        FuelItemEntity fuelItemEntity= ConfigManager.fuelItemMap.get(fuelName);
        if(fuelItemEntity==null){
            throw new LaLaMeowException("锻造石不存在！");
        }
        ItemStack itemStack=new ItemStack(Material.getMaterial(fuelItemEntity.getId()));
        ItemMeta itemMeta=itemStack.getItemMeta();
        itemMeta.setDisplayName(fuelItemEntity.getFuelItemTranName());
        itemMeta.setLore(PluginUtils.translateColorSignList(fuelItemEntity.getLore()));
        itemStack.setItemMeta(itemMeta);
        itemStack.setAmount(count);
        player.getInventory().addItem(itemStack);
        LaLaMeowMessage.sendSuccess(sender,"锻造石头成功给予！");
    }


}
