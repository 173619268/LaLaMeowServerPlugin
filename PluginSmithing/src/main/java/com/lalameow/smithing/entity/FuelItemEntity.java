package com.lalameow.smithing.entity;

import com.lalameow.common.exception.LaLaMeowException;
import com.lalameow.smithing.enumeration.FuelOperType;
import lombok.Data;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/7
 * 时间: 18:36
 * 功能：请进行修改
 */
@Data
public class FuelItemEntity {


    private Integer id;//物品ID
    private String cusname;//自定义名称
    private FuelOperType operType;//锻造石操作类型
    private Double rate=100.0;//成功几率
    private Double rateDM=0.0;//成功率递减系数
    private List<String> lore=new ArrayList<String>();//锻造石描述信息
    private String NBTLable="";//添加或删除的NBT节点名称
    private String NBTType="";//NBT节点值的类型
    private String NBTValue="";//NBT节点的值


    public String getFuelItemName() throws LaLaMeowException {
        if(cusname==null){
            throw new LaLaMeowException("物品名称不能为空");
        }
        return ChatColor.stripColor( ChatColor.translateAlternateColorCodes('&', cusname));
    }
    public String getFuelItemTranName() throws LaLaMeowException {
        if(cusname==null){
            throw new LaLaMeowException("物品名称不能为空");
        }
        return  ChatColor.translateAlternateColorCodes('&', cusname);
    }

}
