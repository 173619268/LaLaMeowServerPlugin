package com.lalameow.smithing.entity;

import com.lalameow.common.exception.LaLaMeowException;
import com.lalameow.smithing.enumeration.EquimentType;
import lombok.Data;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/3/7
 * 时间: 18:36
 * 功能：请进行修改
 */
@Data
public class EquimentEntity {

    private Integer id;//物品ItemID
    private String cusname;//自定义名称
    private EquimentType eqType;//装备类型
    private boolean canHole=false;//是否可以打孔
    private boolean canStrengthen=false;//是否可以强化
    private boolean canUpgrade=false;//是否可以升级
    private Integer maxhole=-1;//最大孔数量
    private List<String> holeTypes=new ArrayList<String>();//允许的孔类型
    private Integer maxStrengthen=-1;//最高强化等级

    private Integer upgradeCount=-1;//升级需要材料的数量
    private String upgradeFuel="";//升级所需材料的名称
    private String upgradeItemName="";//升级后的武器名称

    private List<String> lore=new ArrayList<String>();//装备属性描述信息

    public String getEqumentName() throws LaLaMeowException {
        if(cusname==null){
            throw new LaLaMeowException("物品名称不能为空");
        }
        return ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', cusname));
    }
    public String getEqumentTranName() throws LaLaMeowException {
        if(cusname==null){
            throw new LaLaMeowException("物品名称不能为空");
        }
        return ChatColor.translateAlternateColorCodes('&', cusname);
    }


    public static  void getEqByItemStack(ItemStack itemStack){
        EquimentEntity equimentEntity=new EquimentEntity();
        equimentEntity.setId(itemStack.getType().getId());
        if(itemStack.getItemMeta()!=null){
            if(itemStack.getItemMeta().getDisplayName()!=null){
                equimentEntity.setCusname(itemStack.getItemMeta().getDisplayName());
            }
            if(itemStack.getItemMeta().getLore()!=null){
                equimentEntity.setLore(itemStack.getItemMeta().getLore());
            }
        }

    }
}
