package com.lalameow.skintags.command;

import com.google.gson.JsonParser;
import com.lalameow.common.command.AbstractCommand;
import com.lalameow.skintags.api.SkinJsonAPI;
import com.lalameow.skintags.api.SkinNBTAPI;
import com.lalameow.skintags.SkinTags;
import com.lalameow.skintags.skin.SkinType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Author: SettingDust.
 * Date: 2018/3/28.
 */
public class SkinCommand extends AbstractCommand {
    @Override
    protected void init() {
        command = "skintags";
        commands.put("setitem [类型] [皮肤]", "设置物品皮肤");
        commands.put("setentity [类型] [皮肤]", "设置实体皮肤");
        commands.put("removeitem", "删除物品皮肤");
        commands.put("removeentity [类型]", "删除实体皮肤");
        commands.put("reload", "重载插件");
    }

    @Override
    protected void showHelp() {
        for (String s : commands.keySet()) {
            sender.sendMessage(ChatColor.GREEN + "/" + s + " " + ChatColor.BOLD + commands.get(s));
        }
    }

    protected void reload_cmd() {
        SkinJsonAPI.init();
        sender.sendMessage(ChatColor.GREEN + "重载成功");
    }

    protected void setitem_cmd() {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            ItemStack itemStack = player.getEquipment().getItemInMainHand();
            if (itemStack != null && !itemStack.getType().equals(Material.AIR)) {
                if (SkinJsonAPI.containsType(args[2])) {
                    SkinNBTAPI.setSkin(args[3], SkinType.valueOf(args[2].toUpperCase()), itemStack);
                    sender.sendMessage(ChatColor.GREEN + "设置成功");
                } else {
                    sender.sendMessage(ChatColor.RED + "类型不合法!");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "请手持想设置皮肤的物品!");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "该指令必须由玩家执行!");
        }
    }

    protected void setentity_cmd() {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (SkinJsonAPI.containsType(args[2])) {
                SkinTags.getSetMap().put(player.getUniqueId(),
                        new String[]{args[3], args[2]});
                sender.sendMessage(ChatColor.GREEN + "请右键想要设置的实体");
            } else {
                sender.sendMessage(ChatColor.RED + "类型不合法!");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "该指令必须由玩家执行!");
        }
    }

    protected void removeitem_cmd() {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            ItemStack itemStack = player.getEquipment().getItemInMainHand();
            if (itemStack != null && !itemStack.getType().equals(Material.AIR)) {
                SkinNBTAPI.removeSkin(itemStack);
                sender.sendMessage(ChatColor.GREEN + "删除成功");
            } else {
                sender.sendMessage(ChatColor.RED + "请手持想删除皮肤的物品!");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "该指令必须由玩家执行!");
        }
    }

    protected void removeentity_cmd() {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (SkinJsonAPI.containsType(args[2])) {
                SkinTags.getRemoveMap().put(player.getUniqueId(), args[2]);
                sender.sendMessage(ChatColor.GREEN + "请右键想要设置的实体");
            } else {
                sender.sendMessage(ChatColor.RED + "类型不合法!");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "该指令必须由玩家执行!");
        }
    }
}
