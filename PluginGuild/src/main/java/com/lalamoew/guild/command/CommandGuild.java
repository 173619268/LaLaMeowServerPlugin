package com.lalamoew.guild.command;

import com.lalameow.common.command.AbstractCommand;
import org.bukkit.ChatColor;

/**
 * Author: SettingDust
 * Date: 2017/8/21.
 */
public class CommandGuild extends AbstractCommand {
    @Override
    protected void init() {
        command = "guild";
        commands.put("reload", "重载插件数据。");
    }

    @Override
    protected void showHelp() {
        sender.sendMessage(ChatColor.GREEN + "公会帮助信息");
        for (String s : commands.keySet()) {
            sender.sendMessage(ChatColor.GREEN + "/" + s + " " + ChatColor.BOLD + commands.get(s));
        }
    }

}
