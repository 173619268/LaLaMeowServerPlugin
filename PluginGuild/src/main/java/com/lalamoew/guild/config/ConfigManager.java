package com.lalamoew.guild.config;

/**
 * Author: SettingDust
 * Date: 2017/8/21.
 */
public class ConfigManager {
    public static ConfigGuild guild;
    public static ConfigMain config;

    public static void init() {
        config = new ConfigMain();
        guild = new ConfigGuild();
    }
}
