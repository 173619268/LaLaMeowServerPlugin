package com.lalamoew.guild.config;

import com.lalameow.common.config.AbstractConfig;
import com.lalamoew.guild.Guild;
import org.bukkit.configuration.InvalidConfigurationException;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Author: SettingDust
 * Date: 2017/8/21.
 */
public class ConfigMain extends AbstractConfig {
    protected ConfigMain() {
        super("config.yml", null, Guild.instance);
    }

    public void createDefault() {
        try {
            this.getConfigFile().load(new InputStreamReader(Guild.instance.getResource(this.getFileName()), "UTF-8"));
            this.save();
        } catch (IOException e) {
            e.printStackTrace();
            Guild.instance.getLogger().warning("config.yml created is failed. ");
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            Guild.instance.getLogger().warning("config.yml created is failed. ");
        }
    }

    public void initConfig() {

    }
}
