package com.lalamoew.guild;

import com.lalamoew.guild.command.CommandGuild;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Author: SettingDust
 * Date: 2017/7/14.
 */
public class Guild extends JavaPlugin {
    public static Guild instance;

    @Override
    public void onLoad() {
        instance = this;

        //创建数据
        this.getDataFolder().mkdirs();

    }

    @Override
    public void onEnable() {
        //注册命令
        this.getCommand("guild").setExecutor(new CommandGuild());

    }

    @Override
    public void onDisable() {
    }
}
