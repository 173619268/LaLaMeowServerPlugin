package com.lalameow.common.gui;

import com.google.gson.Gson;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 *
 * @see PositionComponent
 * @see PositionTexture
 * @see PositionSlot
 */
public class Position {
    int x;
    int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position location = (Position) o;
        return x == location.x &&
                y == location.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
