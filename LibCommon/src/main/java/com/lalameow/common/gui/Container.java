package com.lalameow.common.gui;

import java.util.ListIterator;

/**
 * The Container of Component
 *
 * @author SettingDust.
 * Date 2018/5/24
 *
 * @see Gui
 */
public interface Container extends Iterable<Component> {

    /**
     * Returns the size of the container
     *
     * @return The size of the container
     */
    int getSize();

    /**
     * Get all {@link Component}s in the Container.
     *
     * @return An array of all {@link Component}s in the Container.
     */
    Component[] getComponents();

    /**
     * Add a {@link Component} to {@link Container}
     *
     * @param component the {@link Component} you want to add
     */
    void add(Component component);

    /**
     * Return the {@link Component} of the index.
     *
     * @param index The index to get
     * @return {@link Component} of the index.
     */
    Component get(int index);

    /**
     * Stores the {@link Component} at the given index of the container.
     *
     * @param index     The index where to put the {@link Component}
     * @param component The {@link Component} to set
     */
    void set(int index, Component component);

    /**
     * Removes all {@link Component}s in the {@link Container} matching the given type.
     *
     * @param type The type to remove
     */
    void remove(ComponentType type);

    /**
     * Removes the {@link Component} of the index.
     *
     * @param index The index to remove
     */
    void remove(int index);

    /**
     * Removes the {@link Component}.
     *
     * @param component The component to remove
     */
    void remove(Component component);

    /**
     * Checks if the Container contains any {@link Component}s.
     *
     * @param component The {@link Component} to check for.
     * @return true if a {@link Component}s is found.
     */
    boolean contains(Component component);

    /**
     * Clear the {@link Container}
     */
    void clear();

    @Override
    ListIterator<Component> iterator();
}
