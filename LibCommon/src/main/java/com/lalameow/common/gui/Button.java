package com.lalameow.common.gui;

import com.sun.istack.internal.Nullable;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public class Button extends Component {
    private int id;
    private ResourceSound sound;
    private String title;

    public Button(PositionComponent position, ResourceTexture texture, int z, int id, ResourceSound sound, String title) {
        super(position, texture, ComponentType.BUTTON, z);
        this.id = id;
        this.sound = sound;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ResourceSound getSound() {
        return sound;
    }

    public void setSound(@Nullable ResourceSound sound) {
        this.sound = sound;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Button)) return false;
        if (!super.equals(o)) return false;
        Button that = (Button) o;
        return id == that.id &&
                Objects.equals(sound, that.sound) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), id, sound, title);
    }
}
