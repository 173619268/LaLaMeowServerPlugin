package com.lalameow.common.gui;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public class PositionSlot extends Position {

    public PositionSlot(int x, int y) {
        super(x, y);
    }

    public PositionSlot(int slot) {
        this(slot % 9 + 1, slot / 9 + 1);
    }

    /**
     *
     * @return index of the slot
     */
    public int toIndex() {
        int slot = 0;
        slot += x - 1;
        slot += (y - 1) * 9;
        return slot;
    }
}
