package com.lalameow.common.gui;

import com.google.gson.Gson;

import java.util.Objects;

/**
 * @author SettingDust.
 * Date: 2018/5/23.
 * @see Gui
 * @see ISlot
 * @see Button
 */
public abstract class Component {
    private PositionComponent position;
    private ResourceTexture texture;
    private final ComponentType type;

    public Component(PositionComponent position, ResourceTexture texture, ComponentType type) {
        this.position = position;
        this.texture = texture;
        this.type = type;
    }

    public ComponentType getType() {
        return type;
    }

    public PositionComponent getPosition() {
        return position;
    }

    public void setPosition(PositionComponent position) {
        this.position = position;
    }

    public ResourceTexture getTexture() {
        return texture;
    }

    public void setTexture(ResourceTexture texture) {
        this.texture = texture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Component)) return false;
        Component component = (Component) o;
        return Objects.equals(position, component.position) &&
                Objects.equals(texture, component.texture) &&
                type == component.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(position, texture, type);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
