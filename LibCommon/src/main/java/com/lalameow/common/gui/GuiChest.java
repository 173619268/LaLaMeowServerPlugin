package com.lalameow.common.gui;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/24.
 */
public class GuiChest extends Gui {
    private String title;
    private ISlot[] slots;

    public GuiChest(PositionComponent position, ResourceTexture texture, String title) {
        super(position, texture);
        this.title = title;
        this.slots = new ISlot[0];
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Will overwrite the same position slot.
     *
     * @param iSlot the slot to set.
     */
    public void setSlot(ISlot iSlot) {
        ArrayList<ISlot> list = Lists.newArrayList(slots);
        for (int i = 0; i < list.size(); i++) {
            if (iSlot.getPositionSlot().equals(list.get(i).getPositionSlot())) {
                list.remove(i);
            }
        }
        list.add(iSlot);
        slots = list.toArray(new ISlot[0]);
    }

    /**
     * @param positionSlot the position of slot to remove.
     */
    public void removeSlot(PositionSlot positionSlot) {
        ArrayList<ISlot> list = Lists.newArrayList(slots);
        for (int i = 0; i < list.size(); i++) {
            if (positionSlot.equals(list.get(i).getPositionSlot())) {
                list.remove(i);
            }
        }
        slots = list.toArray(new ISlot[0]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuiChest)) return false;
        if (!super.equals(o)) return false;
        GuiChest guiChest = (GuiChest) o;
        return Objects.equals(title, guiChest.title) &&
                Arrays.equals(slots, guiChest.slots);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(super.hashCode(), title);
        result = 31 * result + Arrays.hashCode(slots);
        return result;
    }
}
