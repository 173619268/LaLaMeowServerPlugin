package com.lalameow.common.gui;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public class ISlot extends Component {
    private PositionSlot positionSlot;
    private boolean canFire;

    public ISlot(PositionComponent position, ResourceTexture texture, PositionSlot positionSlot, boolean canFire) {
        super(position, texture, ComponentType.SLOT);
        this.positionSlot = positionSlot;
        this.canFire = canFire;
    }

    public PositionSlot getPositionSlot() {
        return positionSlot;
    }

    public void setPositionSlot(PositionSlot positionSlot) {
        this.positionSlot = positionSlot;
    }

    public boolean isCanFire() {
        return canFire;
    }

    public void setCanFire(boolean canFire) {
        this.canFire = canFire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ISlot that = (ISlot) o;
        return canFire == that.canFire &&
                Objects.equals(positionSlot, that.positionSlot);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), positionSlot, canFire);
    }
}
