package com.lalameow.common.gui;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public abstract class Gui extends Component {
    private GuiContainer guiContainer;
    private Position guiPosition;

    public Gui(PositionComponent position, ResourceTexture texture, ComponentType type, GuiContainer guiContainer) {
        super(position, texture, type);
        this.guiContainer = guiContainer;
        this.guiPosition = new Position(0, 0);
    }

    public GuiContainer getGuiContainer() {
        return guiContainer;
    }

    public Position getGuiPosition() {
        return guiPosition;
    }

    public void setGuiPosition(Position guiPosition) {
        this.guiPosition = guiPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gui)) return false;
        if (!super.equals(o)) return false;
        Gui gui = (Gui) o;
        return Objects.equals(guiContainer, gui.guiContainer) &&
                Objects.equals(guiPosition, gui.guiPosition);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), guiContainer, guiPosition);
    }
}
