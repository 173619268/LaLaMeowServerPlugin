package com.lalameow.common.gui.bukkit;

import com.google.gson.Gson;
import com.lalameow.common.gui.PositionSlot;
import com.lalameow.common.item.EntityItem;

import java.util.Objects;

/**
 * @author SettingDust.
 * Date 2018/6/1.
 */
public class BukkitItemChest {
    private EntityItem item;
    private boolean canMove;
    private PositionSlot positionSlot;

    public BukkitItemChest(EntityItem item, boolean canMove, PositionSlot positionSlot) {
        this.item = item;
        this.canMove = canMove;
        this.positionSlot = positionSlot;
    }

    public EntityItem getItem() {
        return item;
    }

    public void setItem(EntityItem item) {
        this.item = item;
    }

    public boolean isCanMove() {
        return canMove;
    }

    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
    }

    public PositionSlot getPositionSlot() {
        return positionSlot;
    }

    public void setPositionSlot(PositionSlot positionSlot) {
        this.positionSlot = positionSlot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BukkitItemChest)) return false;
        BukkitItemChest that = (BukkitItemChest) o;
        return canMove == that.canMove &&
                Objects.equals(item, that.item) &&
                Objects.equals(positionSlot, that.positionSlot);
    }

    @Override
    public int hashCode() {

        return Objects.hash(item, canMove, positionSlot);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
