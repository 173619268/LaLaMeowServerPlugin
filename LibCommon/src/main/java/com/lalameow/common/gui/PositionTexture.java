package com.lalameow.common.gui;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public class PositionTexture extends PositionComponent {
    private int textureX;
    private int textureY;
    private int textureWidth;
    private int textureHeight;

    public PositionTexture(int x, int y, int width, int height, int textureX, int textureY, int textureWidth, int textureHeight) {
        super(x, y, width, height);
        this.textureX = textureX;
        this.textureY = textureY;
        this.textureWidth = textureWidth;
        this.textureHeight = textureHeight;
    }

    public int getTextureX() {
        return textureX;
    }

    public void setTextureX(int textureX) {
        this.textureX = textureX;
    }

    public int getTextureY() {
        return textureY;
    }

    public void setTextureY(int textureY) {
        this.textureY = textureY;
    }

    public int getTextureWidth() {
        return textureWidth;
    }

    public void setTextureWidth(int textureWidth) {
        this.textureWidth = textureWidth;
    }

    public int getTextureHeight() {
        return textureHeight;
    }

    public void setTextureHeight(int textureHeight) {
        this.textureHeight = textureHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PositionTexture that = (PositionTexture) o;
        return textureX == that.textureX &&
                textureY == that.textureY &&
                textureWidth == that.textureWidth &&
                textureHeight == that.textureHeight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), textureX, textureY, textureWidth, textureHeight);
    }
}
