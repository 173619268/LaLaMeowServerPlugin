package com.lalameow.common.gui;

import com.google.gson.Gson;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public class ResourceTexture extends Resource {
    protected PositionTexture position;

    public ResourceTexture(String path, PositionTexture position) {
        super(path);
        this.position = position;
    }

    public PositionTexture getPosition() {
        return position;
    }

    public void setPosition(PositionTexture position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResourceTexture)) return false;
        if (!super.equals(o)) return false;
        ResourceTexture that = (ResourceTexture) o;
        return Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), position);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
