package com.lalameow.common.gui;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.ListIterator;

/**
 * @author SettingDust.
 * Date 2018/6/1.
 */
public class GuiContainer implements Container {
    private Component[] components;

    /**
     * Returns the size of the container
     *
     * @return The size of the container
     */
    public int getSize() {
        return components.length;
    }

    /**
     * Get all {@link Component}s in the Container.
     *
     * @return An array of all {@link Component}s in the Container.
     */
    public Component[] getComponents() {
        return components;
    }

    /**
     * Add a {@link Component} to {@link Container}
     *
     * @param component the {@link Component} you want to add
     */
    public void add(Component component) {
        List<Component> componentList = Lists.newArrayList(components);
        componentList.add(component);
        components = componentList.toArray(new Component[0]);
    }

    /**
     * Return the {@link Component} of the index.
     *
     * @param index The index to get
     * @return {@link Component} of the index.
     */
    public Component get(int index) {
        return components[index];
    }

    /**
     * Stores the {@link Component} at the given index of the container.
     *
     * @param index     The index where to put the {@link Component}
     * @param component The {@link Component} to set
     */
    public void set(int index, Component component) {
        components[index] = component;
    }

    /**
     * Removes all {@link Component}s in the {@link Container} matching the given type.
     *
     * @param type The type to remove
     */
    public void remove(ComponentType type) {
        List<Component> componentList = Lists.newArrayList(components);
        for (int i = 0; i < componentList.size(); i++) {
            if (componentList.get(i).getType() == type) {
                componentList.remove(i);
            }
        }
        components = componentList.toArray(new Component[0]);
    }

    /**
     * Removes the {@link Component} of the index.
     *
     * @param index The index to remove
     */
    public void remove(int index) {
        List<Component> componentList = Lists.newArrayList(components);
        componentList.remove(index);
        components = componentList.toArray(new Component[0]);
    }

    /**
     * Removes the {@link Component}.
     *
     * @param component The component to remove
     */
    public void remove(Component component) {
        List<Component> componentList = Lists.newArrayList(components);
        componentList.remove(component);
        components = componentList.toArray(new Component[0]);
    }

    /**
     * Checks if the Container contains any {@link Component}s.
     *
     * @param component The {@link Component} to check for.
     * @return true if a {@link Component}s is found.
     */
    public boolean contains(Component component) {
        for (Component component1 : components) {
            if (component1.equals(component)) return true;
        }
        return false;
    }

    /**
     * Clear the {@link Container}
     */
    public void clear() {
        components = new Component[0];
    }

    @Override
    public ListIterator<Component> iterator() {
        return new ContainerIterator(this);
    }
}
