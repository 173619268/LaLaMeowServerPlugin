package com.lalameow.common.gui;

/**
 * Author: SettingDust.
 * Date: 2018/5/24.
 */
public enum ComponentType {
    CUSTOM(-1, -1),
    GUI(-1, -1),
    BUTTON(200, 20),
    SLOT(16, 16);
    private final int width;
    private final int height;

    ComponentType(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
