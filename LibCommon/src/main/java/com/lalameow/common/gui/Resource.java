package com.lalameow.common.gui;

import com.google.gson.Gson;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 *
 * @see ResourceSound
 * @see ResourceTexture
 */
public abstract class Resource {
    protected String path;

    public Resource(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resource that = (Resource) o;
        return Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {

        return Objects.hash(path);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
