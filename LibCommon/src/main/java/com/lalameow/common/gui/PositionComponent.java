package com.lalameow.common.gui;

import java.util.Objects;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public class PositionComponent extends Position {
    private int width;
    private int height;

    public PositionComponent(int x, int y, int width, int height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PositionComponent that = (PositionComponent) o;
        return width == that.width &&
                height == that.height;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), width, height);
    }
}
