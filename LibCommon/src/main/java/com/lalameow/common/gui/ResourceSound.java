package com.lalameow.common.gui;

import com.sun.istack.internal.Nullable;

/**
 * Author: SettingDust.
 * Date: 2018/5/23.
 */
public class ResourceSound extends Resource {
    public ResourceSound(@Nullable String path) {
        super(path);
    }
}
