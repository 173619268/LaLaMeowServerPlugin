package com.lalameow.common.item;


import lombok.Data;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: SettingDust
 * Date: 2017/7/6.
 */
// TODO: 2018/6/1 修改结构
@Data
public class EntityItem implements ConfigurationSerializable {
    private ItemStack itemStack;

    public EntityItem(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public EntityItem(Map<String, Object> map) {
        itemStack = (ItemStack) map.get("itemstack");
    }

    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("itemstack", itemStack);
        return map;
    }
}
