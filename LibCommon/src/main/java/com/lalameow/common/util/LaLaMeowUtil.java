package com.lalameow.common.util;

/**
 * Author: SettingDust
 * Date: 2017/7/10.
 */
public class LaLaMeowUtil {
    public static boolean random(double chance) {
        return (int) (Math.random() * 100) < chance;
    }

    public static int random() {
        return (int) (Math.random() * 100);
    }
}
