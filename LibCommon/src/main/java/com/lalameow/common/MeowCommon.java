package com.lalameow.common;

import com.lalameow.common.item.EntityItem;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2017/6/20
 * 时间: 7:47
 * 功能：公用库加载
 */
public class MeowCommon extends JavaPlugin {
    private static MeowCommon instance;

    @Override
    public void onEnable() {
        ConfigurationSerialization.registerClass(EntityItem.class);
        instance = this;
        this.getLogger().info("Common库加载成功！");
    }

    public static MeowCommon getInstance() {
        return instance;
    }
}
