package com.lalameow.loginbylogingate.config;

import com.lalameow.common.config.AbstractConfig;
import com.lalameow.loginbylogingate.LoginByLoginGate;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/12
 * 时间: 1:42
 * 功能：请进行修改
 */
public class LoginGateConfig extends AbstractConfig {
    protected LoginGateConfig() {
        super("config.yml", null, LoginByLoginGate.plugin);
    }

    @Override
    public void createDefault() {
        createFromResource();
    }

    @Override
    public void initConfig() {
        ConfigManager.loginGateHost=this.getConfigFile().getString("loginGateHost");
        ConfigManager.loginGatePort=this.getConfigFile().getInt("loginGatePort");
    }
}
