package com.lalameow.loginbylogingate;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.lalameow.loginbylogingate.config.ConfigManager;
import com.lalameow.loginbylogingate.network.ClientState;
import com.lalameow.loginbylogingate.network.ConnectionLoginGate;
import com.lalameow.loginbylogingate.network.LoginGateClient;
import com.lalameow.loginbylogingate.protocol.LoginStarPackHandler;
import com.lalameow.loginbylogingate.protocol.LoginAuthPackHandler;
import com.lalameow.packet.enumtype.VailPlayerState;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.security.PrivateKey;
import java.util.concurrent.TimeUnit;


public final class LoginByLoginGate extends JavaPlugin {
    public static LoginByLoginGate plugin;
    public static PrivateKey privateKey;
    @Getter
    private ProtocolManager protocolManager;
    public static Cache<String,VailPlayerState> playerCache;
    @Getter
    private   LoginGateClient loginGateClient;
    @Getter
    private ConfigManager configManager;
    @Override
    public void onEnable() {
        // Plugin startup logic
        plugin=this;
        this.protocolManager= ProtocolLibrary.getProtocolManager();
        playerCache= CacheBuilder.newBuilder()
                .concurrencyLevel(5)
                .expireAfterAccess(30, TimeUnit.SECONDS)
                .build();
        configManager = new ConfigManager(this);
        loginGateClient=new LoginGateClient();
        this.getLogger().info(ChatColor.GREEN+"LoginGate Client 初始化完毕");
        protocolManager.getAsynchronousManager().registerAsyncHandler(new LoginStarPackHandler()).start();
//        protocolManager.getAsynchronousManager().registerAsyncHandler(new LoginAuthPackHandler()).start();
        protocolManager.addPacketListener(new LoginAuthPackHandler());
        this.getLogger().info(ChatColor.GREEN+"Client StarPack 监听启动成功");
        this.getServer().getScheduler().runTaskAsynchronously(this,new ConnectionLoginGate());

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        LoginGateClient.clientState= ClientState.STOP;
        loginGateClient.disconnection();
    }
}
