package com.lalameow.loginbylogingate.network;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 3:26
 * 功能：请进行修改
 */
public enum ClientState {
    INITIAL,
    START,
    STOP,
    EXCEPTION,
    DISCONNECTION
}
