package com.lalameow.loginbylogingate.network;

import com.lalameow.loginbylogingate.LoginByLoginGate;
import org.bukkit.ChatColor;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/11
 * 时间: 23:43
 * 功能：请进行修改
 */
public class ConnectionLoginGate implements Runnable{

    public ConnectionLoginGate() {

    }

    @Override
    public void run() {
        LoginByLoginGate.plugin.getLoginGateClient().initClient();
        while (LoginGateClient.clientState!=ClientState.STOP){
            try {
                if( LoginGateClient.clientState==ClientState.EXCEPTION || LoginGateClient.clientState==ClientState.DISCONNECTION ){
                    LoginByLoginGate.plugin.getLogger().info(ChatColor.RED +"5秒后开始尝试重新连接登录网关...");
                    Thread.sleep(5000);
                    LoginByLoginGate.plugin.getLoginGateClient().initClient();
                }else {
                    Thread.sleep(10000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
