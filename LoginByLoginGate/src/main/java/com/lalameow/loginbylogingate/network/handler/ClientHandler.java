package com.lalameow.loginbylogingate.network.handler;

import com.lalameow.loginbylogingate.LoginByLoginGate;
import com.lalameow.loginbylogingate.network.ClientState;
import com.lalameow.loginbylogingate.network.LoginGateClient;
import com.lalameow.packet.BasePack;
import com.lalameow.packet.enumtype.PackType;
import com.lalameow.packet.packImpl.PacketAbstract;
import com.lalameow.packet.packImpl.PingPack;
import com.lalameow.packet.packImpl.VaildPlayerRecivePack;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.ChatColor;

import java.security.PrivateKey;
import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/7
 * 时间: 20:37
 * 功能：请进行修改
 */
public class ClientHandler extends SimpleChannelInboundHandler<BasePack> {

    @Getter
    private CountDownLatch latch ;
    public ClientHandler() {
    }

    /**
     * 重置锁
     * @param restlatch
     */
    public void resetLatch(CountDownLatch restlatch){
        latch=restlatch;
    }
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            switch (e.state()) {
                case WRITER_IDLE:
                    PingPack pingMsg=new PingPack();
                    ctx.writeAndFlush(pingMsg);
                    break;
                default:
                    break;
            }
        }
    }
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, BasePack msg) throws Exception {
        if(msg.getPackType()== PackType.MCSERVERCONNECTION){
            LoginByLoginGate.privateKey=(PrivateKey) msg.getKey();
            return;
        }
        PacketAbstract packetAbstract=msg.getPackType().getObject(msg.getBodyContent());
        if(packetAbstract instanceof VaildPlayerRecivePack){
            VaildPlayerRecivePack vaildPlayerRecivePack=(VaildPlayerRecivePack)packetAbstract;
            LoginByLoginGate.playerCache.put(vaildPlayerRecivePack.getPlayerName(),vaildPlayerRecivePack.getVailPlayerState());
            latch.countDown();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LoginGateClient.clientState= ClientState.EXCEPTION;
        LoginByLoginGate.plugin.getLoginGateClient().disconnection();
        LoginByLoginGate.plugin.getLogger().info(ChatColor.RED+"连接异常！！");
        LoginByLoginGate.privateKey=null;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        LoginGateClient.clientState= ClientState.DISCONNECTION;
        LoginByLoginGate.plugin.getLoginGateClient().disconnection();
        LoginByLoginGate.privateKey=null;
        LoginByLoginGate.plugin.getLogger().info(ChatColor.RED+"与登录网关断开连接！！");
    }

}
