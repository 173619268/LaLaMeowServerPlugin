package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;
import io.netty.channel.ChannelHandlerContext;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/11
 * 时间: 9:59
 * 功能：请进行修改
 */
@Data
public class VaildPlayerPack extends PacketAbstract {
    private String playerName;

    @Override
    public BasePack revice(ChannelHandlerContext ctx) {
        return null;
    }
}
