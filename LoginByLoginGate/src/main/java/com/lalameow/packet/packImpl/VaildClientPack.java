package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;
import com.lalameow.packet.pojo.AuthenticateClient;
import io.netty.channel.ChannelHandlerContext;
import lombok.Data;


/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/15
 * 时间: 11:46
 * 功能：请进行修改
 */
@Data
public class VaildClientPack extends PacketAbstract {
    private AuthenticateClient authenticateClient;
    @Override
    public BasePack revice(ChannelHandlerContext ctx) {

        return null;
    }
}
