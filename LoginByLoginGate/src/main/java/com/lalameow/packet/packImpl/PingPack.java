package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;
import com.lalameow.packet.enumtype.PackType;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 3:37
 * 功能：请进行修改
 */
public class PingPack extends PacketAbstract {
    @Override
    public BasePack revice(ChannelHandlerContext ctx) {
        BasePack basePack=new BasePack();
        basePack.setPackType(PackType.PING);
        return basePack;
    }
}
