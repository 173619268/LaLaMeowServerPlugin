package com.lalameow.packet.packImpl;

import com.lalameow.packet.BasePack;
import com.lalameow.packet.enumtype.VailPlayerState;
import io.netty.channel.ChannelHandlerContext;
import lombok.Data;

import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/11
 * 时间: 10:00
 * 功能：请进行修改
 */
@Data
public class VaildPlayerRecivePack extends PacketAbstract {
    private String playerName;
    private VailPlayerState vailPlayerState;
    @Override
    public BasePack revice(ChannelHandlerContext ctx) {

        return null;
    }
}
