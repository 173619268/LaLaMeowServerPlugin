package com.lalameow.packet.packImpl;

import com.google.gson.Gson;
import com.lalameow.packet.BasePack;
import com.lalameow.packet.PackInterface;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/7
 * 时间: 23:55
 * 功能：请进行修改
 */
public abstract class PacketAbstract implements PackInterface {
    @Override
    public String toJsonStr() {
        Gson gson=new Gson();
        return gson.toJson(this);
    }
    public abstract BasePack revice(ChannelHandlerContext ctx);
}
