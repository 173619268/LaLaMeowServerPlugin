package com.lalameow.packet;

import com.lalameow.packet.enumtype.PackType;
import lombok.Data;

import java.io.Serializable;
import java.security.Key;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/7
 * 时间: 21:14
 * 功能：请进行修改
 */
@Data
public class BasePack implements Serializable {
    //必须唯一，否者会出现channel调用混乱
    private String clientId;
    private PackType packType;
    private String bodyContent;
    private Key key;
    private String header;

    @Override
    public String toString() {
        return "BasePack{" +
                "clientId='" + clientId + '\'' +
                ", packType=" + packType +
                ", bodyContent='" + bodyContent + '\'' +
                ", key='" + key + '\'' +
                ", header='" + header + '\'' +
                '}';
    }
}
