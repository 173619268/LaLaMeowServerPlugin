package com.lalameow.packet.enumtype;


import com.google.gson.Gson;
import com.lalameow.packet.PackInterface;
import com.lalameow.packet.packImpl.*;
import lombok.Getter;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/7
 * 时间: 21:15
 * 功能：请进行修改
 */
public enum  PackType {

    PING(PingPack.class),
    VAILDPLAYER(VaildPlayerPack.class),
    VAILDPLAYERRECIVEPACK(VaildPlayerRecivePack.class),
    VAILDCLIENT(VaildClientPack.class),
    MCSERVERCONNECTION(McServerConnectionPack.class);

    @Getter
    private Class<? extends PacketAbstract> packetClass;

    PackType(Class<? extends PacketAbstract> packetClass){
        this.packetClass =packetClass;

    }

    /**
     * 获取json
     * @param packInterface
     * @return
     */
    public String getJsonContent(PackInterface packInterface){
        return  packInterface.toJsonStr();
    }

    /**
     * 获取对象
     * @param json
     * @return
     */
    public PacketAbstract getObject(String json){
        Gson gson=new Gson();
        return  gson.fromJson(json,this.getPacketClass());
    }

}
