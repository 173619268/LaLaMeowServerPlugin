package com.lalameow.packet.enumtype;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/14
 * 时间: 11:31
 * 功能：请进行修改
 */
public enum VailPlayerState {
    SUCESS,
    NOLOGIN,
    TIMEOUT,
    EXCEPTION,
    NOAUTH
}
