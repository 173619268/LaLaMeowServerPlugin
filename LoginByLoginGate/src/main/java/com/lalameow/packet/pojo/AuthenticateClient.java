package com.lalameow.packet.pojo;

import lombok.Data;


/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/15
 * 时间: 11:20
 * 功能：请进行修改
 */

@Data
public class AuthenticateClient {
    private String playerName;
    private String randomKey;
}
