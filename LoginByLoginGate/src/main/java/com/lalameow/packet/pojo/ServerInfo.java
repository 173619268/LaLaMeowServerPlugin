package com.lalameow.packet.pojo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * 创建人: 陈刚
 * 日期: 2018/5/8
 * 时间: 4:10
 * 功能：请进行修改
 */
@Data
public class ServerInfo {
    private String serverHost;
    private String serverName;

    public ServerInfo() {
    }

    public ServerInfo(String serverName,String serverHost) {
        this.serverHost = serverHost;
        this.serverName = serverName;
    }
}
